@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        
        
        <div class="col-6 offset-5  pb-2">
            <a href="{{route('reparaciones.index')}}" class="btn btn-success float-right mr-5" >Listar Partes de Reparación</a>
        </div>
       
    </div>
    <h2 class="pt-2 col-10 offset-1">Actualizar Parte de Reparación</h2>
        <hr class="pt-2 ml-5 mr-5"> 
    <div class="row mt-3 ">
    
        <div class="col-sm-5  offset-1">
            {!! Form::open(array(
                    'route'     =>  ['reparaciones.update',$reparacion->id_reparacion],
                    'method'    =>  'PATCH',
                    'class'     =>  'col-10 offset-1 pa-5',                    
                    'validated')
                )
            !!}
                
                @include('gestion.reparaciones.edit')
                
            {!! Form::close() !!}
         </div> 
         <div class="col-sm-5 ml-5 mb-5">
                <h4 class="text-center mb-3 mb-3"> Tabla Taxis</h4>
                 @include('gestion.turnos.taxis')
       </div>
        <script>
                    
            function manejarCheck(e) {
                var evt = e || window.event;
                var target = evt.target||evt.srcElement;
                var codTaxiTf = document.getElementById('cod_taxi');
                if(target.checked==true){
                    
                    codTaxiTf.value= target.getAttribute('id'); 
                }else{
                    codTaxif.value= ""; 
                }    
                
            } 
            function init(){
                var checks = document.getElementsByClassName('check'); 
                var tam= checks.length;
                for(var indx = 0; indx<tam; indx++){
                    if(checks[indx].addEventListener){
                        checks[indx].addEventListener('click',manejarCheck,false);
                    }else{
                        checks[indx].attachEvent('onclick',manejarCheck);
                    }

                }
                document.getElementById('crearBtn').onclick=function(){
                        document.getElementById('cod_taxi').removeAttribute('disabled');
                }
               
            }    
            
            window.onload= init;
               
        
        
        </script>    
@endsection 