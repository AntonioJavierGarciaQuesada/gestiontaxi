
<div class="row">
        <div class="form-group mb-3 col-6" >
            <label for="cod_taxi">Código Taxi</label>
            <input type="number" name="cod_taxi" class="form-control" value="{{$reparacion->cod_taxi}}" id="cod_taxi" disabled="true" required>    
        </div>
   </div>
        
    <div class="row">    
        <div class="form-group col-6">
            <label for="fecha_entrada" class="mr-3">Fecha Entrada</label>
            <input type="date" name="fecha_entrada" class="form-control"  value="{{$reparacion->fecha_entrada}}"   required> 
        </div>   
        <div class="form-group col-6">
            <label for="fecha_salida" class="mr-3">Fecha Salida</label>
            <input type="date" name="fecha_salida" class="form-control" value="{{$reparacion->fecha_salida}}" required >   
        </div>
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="fecha_salida" class="mr-3">Fecha Prevista</label>
            <input type="date" class="form-control" name="fecha_prevista" value="{{$reparacion->fecha_prevista}}" >   
        </div>
        <div class="form-group col">
            <label for="coste_reparacion" class="mr-3">Coste Reparacion</label>
            <input type="number" step="0.01" class="form-control" name="coste_reparacion" value="{{$reparacion->coste_reparacion}}" required>
        </div>  
    </div>     
    <div class="container mt-4">
        <div class="row justify-content-center">
            <button class="btn btn-primary" id="crearBtn" >Actualizar Parte</button>
        </div>
    </div>
    
