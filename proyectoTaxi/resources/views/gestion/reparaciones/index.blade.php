@extends('layouts.app')

@section('content')
    <div class="row mt-5">
      
    <div class="col-10 offset-1">
        <a href="reparaciones/create" class="btn btn-success btn-md float-right mr-5 " >Crear Parte Reparación</a>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1"> Listar Partes Reparación</h2>
    <hr class="pt-2 pb-2 col-10 offset-1">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr >
                    <th>Id. Parte Reparación</th>
                    <th>Id. Taxi</th>                    
                    <th>Fecha Entrada</th>
                    <th>Fecha Salida</th>
                    <th>Coste Reparación</th>                    
                    <th>Acción</th>      
                </tr> 
                @foreach($reparacionesListado as $reparacion)
                <tr>
                    <td>{{$reparacion->id_reparacion}}</td> 
                    <td>{{$reparacion->cod_taxi}}</td>                  
                    <td>{{$reparacion->fecha_entrada}}</td>
                    <td>{{$reparacion->fecha_salida}}</td>                    
                    <td>{{$reparacion->coste_reparacion}}</td>                    
                    <td>
                        <a href="{{route('reparaciones.show',$reparacion->id_reparacion)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('reparaciones.edit',$reparacion->id_reparacion)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['reparaciones.destroy',
                                                $reparacion->id_reparacion                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $reparacionesListado->links() !!}
    </div>    
    </div>
@endsection