   <div class="row">
        <div class="form-group mb-3 col" >
            <label for="cod_taxi">Código Taxi</label>
            <input type="number" name="cod_taxi" class="form-control" id="cod_taxi" disabled="true" required>    
        </div>
   </div>
        
        <div class="row">    
        <div class="form-inline form-group col" >
            <label for="fecha_entrada" class="mr-3">Fecha Entrada</label>
            <input type="date" name="fecha_entrada" class="form-control" min="<?php echo date('Y-m-d')?>"  required> 
        </div>   
        <div class="form-inline form-group col">
            <label for="fecha_salida" class="mr-3">Fecha Salida</label>
            <input type="date" name="fecha_salida" class="form-control" min="<?php echo date('Y-m-d')?>" required >   
        </div>
        <div class="form-inline form-group col">
            <label for="fecha_salida" class="mr-3">Fecha Prevista</label>
            <input type="date" class="form-control" name="fecha_prevista" min="<?php echo date('Y-m-d')?>">   
        </div>
        <div class="form-inline form-group col">
            <label for="coste_reparacion" class="mr-3">Coste Reparacion</label>
            <input type="number" step="0.01" class="form-control" name="coste_reparacion" required>
        </div>  
    </div>     
    <div class="container mt-4">
        <div class="row justify-content-center">
            <button class="btn btn-primary" id="crearBtn" >Crear Parte</button>
        </div>
    </div>
    