@extends('layouts.app')

@section('content')
<div class="row">
        
        <div class="col-6 offset-5 mb-4 mt-4">
            <a href="{{route('reparaciones.index')}}" class="btn btn-success float-right mr-5" >Listar Reparaciones</a>
        </div>
        <h2 class="pt-2 pb-2 col-10 offset-1 pa-5 "> Consultar Parte Reparción</h2>
        <hr class="pt-2 pb-2 col-10 offset-1 pa-5 " >
</div>        
    <section class="row col-10 offset-1 mb-2">
        
        <div class="card col-10 offset-1 p-5">                        
            <div class="card-body">                        
                <h4 class="card-title">Datos Reparacion</h4>                
                <hr>
                    <h5> Id Reparacion: {{$reparacion->id_reparacion}}</h5>
                    <hr>
                    <p class="card-text">id. Taxi: {{$reparacion->cod_taxi}} </p>
                    <p class="card-text">Matricula:{{$reparacion->matricula}}</p>
                    <p class="card-text">Fechas </p>
                    <ul class="row">
                        <li>Fecha Entrada: {{$reparacion->fecha_entrada}}</li>
                        <li >Fecha Salida: {{$reparacion->fecha_salida}}</li>
                        <li>Fecha Prevista: {{$reparacion->fecha_prevista}}</li>
                    </ul>
                    <p class="card-text">Coste:{{$reparacion->coste_reparacion}}</p>                               
                    <hr>   
               </div>                                              
        </div>   

    </section>
</div>
@endsection