   <div class="row">
    
    <div class="col-sm-5 mb-5">
        <div class="row">
            <div class="form-group col">
                <label for="cod_estado">Código Estado</label>
                <input type="number" class="form-control " name="cod_estado"  value ="{{$estadoCarrera->cod_estado}}" id="cod_estado" disabled="true" required>        
            </div>  
            <div class="form-group col">
                <label for="cod_carrera">Código Carrera</label>
                <input type="number" class="form-control" value="{{$estadoCarrera->cod_carrera}}" name="cod_carrera"  id="cod_carrera" disabled="true" required>        
            </div>
        </div>    
        <div class="row">
            <div class="form-group col" >
                <label for="duracion" class="mr-3">Fecha </label>
                <input type="date" class="form-control"  value="{{$estadoCarrera->fecha}}" name="fecha">
            </div>
            <div class="form-group  col" >
                <label for="duracion" class="mr-3">Hora </label>
                <input type="time" class="form-control" name="hora" value="{{$estadoCarrera->hora}}">
            </div> 
        </div>         
        <div class="row">
            <div class="form-group col">
                <label for="foto">Foto </label>
                <input name="foto" type="file"> 
            </div> 
        </div>        
        <div class="form-group row justify-content-center">
            <button class="btn btn-primary btn-md" id="crearBtn" >Actualizar Estado Carrera</button>
        </div>       
    </div>

    <div class="col-sm-6 offset-1 mb-5">
        <div class="form-group">
            <label for="visualizar" class="mr-3">Visualiazar la tabla</label>
            <select id="visualizar" class="custom-select">
                <option value="estados">Estado Carrera</option>
                <option value="carrera">Carreras</option>                      
            </select>
        </div>
        <div id="estados" style="" class="div-tabla">
            <?php $estados=$datos[0];?>
            @include('gestion.estadosCarreras.tipoEstado')
        </div> 
        <div id="carreras" style="display:none" >
            <?php $carreras=$datos[1];?> 
            @include('gestion.estadosCarreras.carreras')              
        </div> 
    </div> 
</div>       
    
   
