@extends('layouts.app')

@section('content')
<div class="row mt-5">
        
        <div class="col-6 offset-5 ">
            <a href="{{route('carreras.index')}}" class="btn btn-success float-right mr-5" >Listar Carreras</a>
        </div>
</div>    
<h2 class="pt-2 pb-2 col-10 offset-1 pa-5 "> Consultar Estado Carrera</h2>    
<hr class="pt-2 pb-2 col-10 offset-1 pa-5 ">
    <section class="row mb-4">
        
        <div class="card col-10 offset-1 p-4">                        
            <div class="card-body">                        
                <h4 class="card-title">Datos Estado Carrera</h4>                
                <hr>
                    <h5> Id Estado Carrera: {{$estadoCarrera->id_estado_carrera}}</h5>
                    <p class="card-text">
                        Id. Estado Carrera:{{$estadoCarrera->cod_estado}}
                    </p>

                    <p class="card-text">
                        Id. Carrera: {{$estadoCarrera->cod_carrera}}
                    <p> 
                    <p class="card-text">
                        <span>Fecha:{{$estadoCarrera->fecha}}</span>
                        <span>Hora:{{$estadoCarrera->hora}}</span>                      
                    <p>
                      <hr>                
                                           
               </div>                                              
            </div>                                                          
            
        </div>

             

    </section>
</div>
@endsection