@extends('layouts.app')

@section('content')
    <div class="row mt-5">
    
    
    <div class="col-10 offset-1">
        
        <a href="estadosCarreras/create" class="btn btn-success float-right mr-5" >Crear Estado Carrera</a>
    </div>
    <h2 class=" pb-2 col-10 offset-1"> Listar Estados De Las Distintas Carreras</h2>
    <hr class="pt-2 pb-2 col-10 offset-1">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr>
                    <th>Id Estado Carrera</th>
                    <th>Id Carrera</th>                                   
                    <th>Estado</th>
                    <th>Fecha</th>
                    <th>Hora</th>                    
                    <th>Acción</th>                        
                </tr> 
                @foreach($estadosCarreras as $estadoCarrera)
                <tr>
                    <td>{{$estadoCarrera->id_estado_carrera}}</td>
                    <td>{{$estadoCarrera->cod_carrera}}</td>
                    <td><?php if($estadoCarrera->cod_estado=="1"){echo "Ocupado";}else{echo "Libre";}?></td>                   
                    <td>{{$estadoCarrera->fecha}}</td>
                    <td>{{$estadoCarrera->hora}}</td>                                      
                    <td>
                        <a href="{{route('estadosCarreras.show',$estadoCarrera->id_estado_carrera)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>                      
                        <a href="{{route('estadosCarreras.edit',$estadoCarrera->id_estado_carrera)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['estadosCarreras.destroy',
                                                $estadoCarrera->id_estado_carrera                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                        
                        
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $estadosCarreras->links() !!}
    </div>    
    </div>
@endsection