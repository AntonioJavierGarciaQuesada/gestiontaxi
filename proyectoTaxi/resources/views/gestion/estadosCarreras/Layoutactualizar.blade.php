@extends('layouts.app')

@section('content')
        <div class="row mt-5">           
            
            <div class="col-6 offset-5 ">
                <a href="{{route('estadosCarreras.index')}}" class="btn btn-success float-right mr-5" >Listar Estados Carreras</a>
            </div>
        </div>
        <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Actualizar Estado Carrera</h2>
        <hr>    
        <div class="row">
             <div class="col-xs-12 col-sm-12 mt-5">
             {!! Form::open(array(
                'route'     =>  ['estadosCarreras.update',$estadoCarrera->id_estado_carrera],
                'method'    =>  'PATCH',
                'class'     =>  'col-10 offset-1 pa-5',
                'Validated')
            )
        !!}
                    @include('gestion.estadosCarreras.edit')
                
                {!! Form::close() !!}
            </div> 
            

        </div>
        <script>
                    
                    function manejarCheck(e) {
                        var evt = e || window.e;
                        var target = evt.target||evt.srcElement;
                        var codEstadoTf = document.getElementById('cod_estado');               
                        var codCarreraTf = document.getElementById('cod_carrera');                         
                        var tabla =target.parentElement.parentElement.parentElement.parentElement;
                        var tabla = tabla.getAttribute('id');
                        
                        if(target.checked==true){
                            if(tabla=="tablaTipoEstado") {
                                codEstadoTf.value= target.getAttribute('id');
                            }else{
                                codCarreraTf.value= target.getAttribute('id');
                            }                                 
                                        
                        }else{
                            if(tabla=="tablaTipoEstado"){
                                codEstadoTf.value= "";
                            }else{
                                codCarreraTf.value= "";
                            }    
                                
                        }    
                        
                    } 
        
                    function manejadorVisualizar(e){
                        
                        var evt = e || window.event;
                        var target = evt.target||evt.srcElement;
                        var divSelec = document.getElementsByClassName("div-tabla")[0];
                        divSelec.removeAttribute("class");
                        divSelec.setAttribute("style","display:none;")
                        if(target.value == "estados"){
                            var cambiarDiv=document.getElementById("estados"); 
                        }else{
                            var cambiarDiv=document.getElementById("carreras");      
                        }                     
                        
                        cambiarDiv.setAttribute("class","div-tabla");
                        cambiarDiv.removeAttribute("style"); 
                    }

                    function init(){
                        var checks = document.getElementsByClassName('check'); 
                        var select = document.getElementById('visualizar');
                        if(select.addEventListener){
                            select.addEventListener("change",manejadorVisualizar,false);
                        }else{
                            select.attchEvent("onchage",manejadorVisualizar);
                        }
                        
                        var tam= checks.length;
                        for(var indx = 0; indx<tam; indx++){
                            if(checks[indx].addEventListener){
                                checks[indx].addEventListener('click',manejarCheck,false);
                            }else{
                                checks[indx].attachEvent('onclick',manejarCheck);
                            }
        
                        }

                        document.getElementById('crearBtn').onclick=function(){
                                
                                document.getElementById('cod_estado').removeAttribute('disabled');
                                document.getElementById('cod_carrera').removeAttribute('disabled');
                        }
                       
                    }    
                    
                    window.onload= init;
                       
                
                
                </script> 
@endsection   