@extends('layouts.app')

@section('content')
    <div class="row mt-5">   
    
    <div class="col-10 offset-1  pb-2">
        <a href="tipoEstado/create" class="btn btn-md btn-success float-right mr-5  " >Crear Tipo Estado</a>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1"> Listar Tipos Estado de una Carrera</h2>
    <hr class="pt-2 pb-2 col-10 offset-1">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr >
                    <th>Id. Tipo Estado</th>
                    <th>Tipo</th>
                    <th>Descripcion</th>                    
                    <th>Acción</th>                          
                </tr> 
                @foreach($tipoEstados as $tipoEstado)
                <tr>
                    <td>{{$tipoEstado->id_tipo_estado}}</td>
                    <td>{{$tipoEstado->tipo}}</td>
                    <td>{{$tipoEstado->descripcion}}</td>                                        
                    <td>
                        <a href="{{route('tipoEstado.show',$tipoEstado->id_tipo_estado)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('tipoEstado.edit',$tipoEstado->id_tipo_estado)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['tipoEstado.destroy',
                                                $tipoEstado->id_tipo_estado                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $tipoEstados->links() !!}
    </div>    
    </div>
@endsection