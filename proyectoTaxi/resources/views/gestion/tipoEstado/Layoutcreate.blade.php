@extends('layouts.app')

@section('content')
    
        <div class="row mt-5">
            <div class="col-6 offset-5 ">
                <a href="{{route('tipoEstado.index')}}" class="btn btn-success btn-md float-right mr-5" >Listar Tipo Estado</a>
            </div>
        </div>
        
        
        <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Añadir Tipo Estado de una carrera</h2>
        <hr class="pt-2 pb-2 col-10 offset-1">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>    
        {!! Form::open(array(
                'route'     =>  'tipoEstado.store',
                'method'    =>  'POST',
                'class'     =>  'col-10 offset-1 mt-4',
                'validated' => 'true')
            )
        !!}
            @include('gestion.tipoEstado.create')
        
        {!! Form::close() !!}
      
@endsection   