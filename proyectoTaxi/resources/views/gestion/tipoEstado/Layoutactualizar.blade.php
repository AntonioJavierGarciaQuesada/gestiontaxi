@extends('layouts.app')

@section('content')
    <div class="row mt-5">
    
    
    <div class="col-10 offset-1 mt-2">
        <a href="{{route('tipoEstado.index')}}" class="btn btn-success col-xs-10 offset-xs-1 col-xs-10 offset-xs-1 col-sm-5 offset-sm-6 col-md-2 offset-md-9" >Listar Tipo Estado</a>
    </div>

    <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Actualizar Tipo Estado de una Carrera</h2>
    <hr class="pt-2 pb-2 col-10 offset-1 pa-5">
    {!! Form::open(array(
            'route'     =>  ['tipoEstado.update',$tipoEstado->id_tipo_estado],
            'method'    =>  'PATCH',
            'class'     =>  'col-10 offset-1 pa-5')
        )
    !!}
        
        @include('gestion.tipoEstado.edit')
        
    {!! Form::close() !!}
   </div>    
@endsection 