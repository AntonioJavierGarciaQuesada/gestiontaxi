    
    <div class="form-group mb-3 col-6" >
        <label for="tipo" class="mr-2">Tipo</label>
        <input type="text" class="form-control" name="tipo" required>
    </div>   
    <div class="form-group col">
        <label for="descripcion">Descripción</label>
        <input type="textarea" class="form-control" name="descripcion" required>
    </div>
  
    <div class="row justify-content-center mt-5">
        <button class="btn btn-primary">Crear Tipo Estado</button>
    </div>