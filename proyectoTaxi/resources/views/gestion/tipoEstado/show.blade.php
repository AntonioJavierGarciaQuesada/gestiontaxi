@extends('layouts.app')

@section('content')
<div class="row mt-4">
       
        <div class="col-6 offset-5 mt-4">
            <a href="{{route('tipoEstado.index')}}" class="btn btn-success float-right mr-5">Listar Tipo Estado</a>
        </div>
        <h2 class="pt-2 pb-2 col-10 offset-1  "> Consultar Tipo Estado de una Carrera</h2>
        <hr class="pt-2 pb-2 col-10 offset-1 ">
    <section class="col-8 offset-2">
        <div class="card p-5">                        
            <div class="card-body">                        
                <h4 class="card-title">Ficha Tipo Estado</h4> 
                <hr>        
                <h5> Id. Tipo Estado : {{$tipoEstado->id_tipo_estado}}</h5>
                <hr>
                <p class="card-text">Tipo: {{$tipoEstado->tipo}} </p>
                <p class="card-text">Descripcion: {{$tipoEstado->descripcion}}</p>  
                <hr>                                    
            </div>
    </section>
</div>
@endsection