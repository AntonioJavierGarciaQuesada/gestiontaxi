@extends('layouts.app')

@section('content')
    <div class="row mt-5">

    <div class="col-10 offset-1  pb-2">

        <a href="../" class="btn btn-success float-right ml-5" ></a>
    </div>
    <h2 class="col-10 offset-1">Recuperación de Campos</h2>
    <hr class="col-10 offset-1">
    </div>
    <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>      
    <div class="container mb-5">
        <div class="consultas">
              
                {!! Form::open(array(
                        'url'     =>  '/gestion/recuperar',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-5 mt-3',
                        'validated')
                    )
                !!}
                     <div class="form-inline col">
                            <label for="" class="mr-2">Seleccione la tabla a recuperar</label>
                            <select name="tabla" class="custom-select" id="tabla">
                                <option value="taxis" <?php if(isset($datos->tabla)&& $datos->tabla=='taxis'){echo 'selected';};?>>Taxis</option>
                                <option value="empleados" <?php if(isset($datos->tabla)&& $datos->tabla=='empleados'){echo 'selected';};?>>Empleados</option>
                                <option value="tarifas" <?php if(isset($datos->tabla)&& $datos->tabla=='tarifas'){echo 'selected';};?>>Tarifas</option>
                                <option value="turnos" <?php if(isset($datos->tabla)&& $datos->tabla=='turnos'){echo 'selected';};?>>Turnos</option>
                                <option value="reparaciones" <?php if(isset($datos->tabla)&& $datos->tabla=='recuperaciones'){echo 'selected';};?>>Recuperaciones</option>
                                <option value="carreras" <?php if(isset($datos->tabla)&& $datos->tabla=='carreras'){echo 'selected';};?>>Carreras</option>
                                <option value="tipoEstado" <?php if(isset($datos->tabla)&& $datos->tabla=='tipoEstado'){echo 'selected';};?>>Tipo de Estado de una Carrera</option>
                                <option value="estadoCarreras" <?php if(isset($datos->tabla)&& $datos->tabla=='estadosCarrera'){echo 'selected';};?>>Estado de una Carrera</option>
                            </select>
                      
                    </div>
                    <div class="col">
                       
                            <input type="submit" class="btn btn-info btn-md" value="consultar">
                        
                    </div>

                    {!! Form::close() !!}
            


    <div class="col-10 offset-1 ">
    @if($datos != null && $datos->tabla=="carreras")
        <table class="table table-striped">
                <tr >
                    <th>Id Carrera</th>
                    <th>Tarifa</th>
                    <th>Turno</th>
                    <th>Controlador</th>
                    <th>Origen</th>
                    <th>Destino</th>
                    <th>Acción</th>

                </tr>
                    
                        @foreach($tabla as $carrera)
                            <tr>
                                <td>{{$carrera->id_carrera}}</td>
                                <td>{{$carrera->cod_tarifa}}</td>
                                <td>{{$carrera->cod_turno}}</td>
                                <td>{{$carrera->cod_controlador}}</td>
                                <td>{{$carrera->origen}}</td>
                                <td>{{$carrera->destino}}</td>
                            
                                <td>{!! Form::open([
                                    'method' => 'post',
                                    'route' => ['recuperar.update/'.$datos->tabla.'/'.$carrera->id_carrera.'/'                                                                   
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-primary'>Recuperar</button>
                        {!! Form::close() !!}</td>
                            </tr>

                        @endforeach
                    

                         
                    @endif
        </table>
    </div>
    <div class="col-8 offset-2">

    </div>
    </div>
    </div>
@endsection
<script>

    function manejadorEventoTaxi(e){
       var id_taxi=e.target.value;
       var link = document.getElementById('consultarTaxi');
       link.setAttribute('href','/consultas/consultasCarreras/taxi/'+id_taxi);
    }
    function manejadorEventoCarrera(e){
       var id_carrera=e.target.value;
       var link = document.getElementById('consultarCarrera');
       link.setAttribute('href','/consultas/consultasCarreras/carrera/'+id_carrera);

    }


    function init(){
      var  matriculaSl=document.getElementById('matricula');
      var carreraInput = document.getElementById('carr');
      matriculaSl.onblur = manejadorEventoTaxi;
      carreraInput.onblur = manejadorEventoCarrera;
    }
    window.onload= init;
</script>
