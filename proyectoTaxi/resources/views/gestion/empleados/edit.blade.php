<div class="row mb-3">
    <div class="form-group col">
        <label for="puesto">Puesto</label>
        <select name="puesto" id="puesto" class="custom-select form-control" required>
                <option value="taxista" selected>Taxista</option>
                <option value="controlador">Controlador</option>
        </select>
    </div>
    <div class="form-group col" id="licencia" >
        <label for="num_licencia" class="mr-3">Nº de Licencia</label>
        <input type="text" name="num_Licencia" class="form-control" value="{{$empleado->num_Licencia}}" pattern="^[0-9]{8}[A-Z]{1}$" title="Formato invalida :12345678A" >
    </div>
    <div class="form-group  col">
        <label for="num_seg_social" class="mr-3">Nº segurida Social</label>
        <input type="text" name="num_seg_social" class="form-control" pattern="[0-9]{11}"  value="{{$empleado->num_seg_social}}" title="Formato invalida :12345678901" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-5">
        <label for="nombre" class="">Nombre</label>
        <input type="text" class="form-control" name="nombre" value="{{$empleado->nombre}}" required>
    </div>
    <div class="form-group col-7">
        <label for="apellidos" class="mr-3">Apellidos</label>
        <input type="text" class="form-control" name="apellidos" value="{{$empleado->apellidos}}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="fecha" class="mr-3">Fecha Nacimiento</label>
        <input type="date"  class="form-control" name="fecha_nacimiento"
        max="<?php echo date('Y-m-d', strtotime('-18 year'));?>" min="1960-01-01" value="{{$empleado->fecha_nacimiento}}" required>
    </div>
    <div class="form-group col">
        <label for="fecha" class="mr-3">Fecha Contratación</label>
        <input type="date"  class="form-control" name="fecha_contratacion" value="{{$empleado->fecha_contratacion}}" min="2018-01-01" required>
    </div>
    <div class="form-group col">
        <label for="telefono" class="mr-3">Teléfono</label>
        <input type="number"  class="form-control" name="telefono" min="9" value="{{$empleado->telefono}}" required>    
    </div>
   
    <div class="form-group col">
        <label for="email">Email</label>
        <input type="email"  class="form-control" name="email" value="{{$empleado->email}}" required>
    </div>

</div>
<div class="row">
<div class="form-group col">
        <label for="direccion">Dirección</label>
        <input type="textarea" class="form-control" name="direccion"  value="{{$empleado->direccion}}"required>
    </div>

</div>   
<div class="row">
    <div class="form-group col">
        <label for="situacion">Situacion</label>
        <select name="situacion" id=""  class="custom-select col">
            <option value="activo" <?php if($empleado->situacion=="activo"){echo " selected";}?>>Activo</option>
            <option value="permiso" <?php if($empleado->situacion=="permiso"){echo " selected";}?>>Permiso</option>
            <option value="vacaciones"<?php if($empleado->situacion=="vaciones"){echo " selected";}?>>Vacaciones</option>
            <option value="baja" <?php if($empleado->situacion=="baja"){echo " selected";}?>>Baja</option>
        </select>
    </div>
    <div class="form-group col">
        <label for="foto">Foto </label>
        <input name="foto" type="file" />
    </div>
</div>   
    

    <div class="row justify-content-center">
        <button class="btn btn-primary" id="crearBtn" >Actualizar Empleado</button>
    </div>
    