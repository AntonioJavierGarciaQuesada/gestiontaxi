@extends('layouts.app')

@section('content')
    <div class="row mt-5">
    
    
    <div class="col-10 offset-1 ">
        <a href="empleados/create" class="btn btn-success float-right mr-5" >Crear Empleado</a>
    </div>
    <h2 class="pt-4 pb-2 col-10 offset-1"> Listar Empleados</h2>
    <hr class="pt-4 pb-2 col-10 offset-1">
   
    
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr >
                    <th>Cod. Empleado </th>                    
                    <th>Num. Seguridad Social</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Teléfono</th>
                    <th>Situación</th>
                    <th>Puesto</th>
                    <th>Acción</th>        
                </tr> 
                @foreach($empleadosCompleto as $empleado)
                <tr>
                    <td>{{$empleado->id_empleado}}</td>
                    <td>{{$empleado->num_seg_social}}</td>
                    <td>{{$empleado->nombre}}</td>
                    <td>{{$empleado->apellidos}}</td>
                    <td>{{$empleado->telefono}}</td>
                    <td>{{$empleado->situacion}}</td>
                    <td>{{$empleado->puesto}}</td>                    
                    <td>
                        <a href="{{route('empleados.show',$empleado->id_empleado)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('empleados.edit',$empleado->id_empleado)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['empleados.destroy',
                                                $empleado->id_empleado                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $empleadosCompleto->links() !!}
    </div>    
    </div>
@endsection