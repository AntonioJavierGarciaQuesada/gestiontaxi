@extends('layouts.app')

@section('content')
<div class="row mt-5">
       
        <div class="col-6 offset-5 mb-4 mt-4">
            <a href="{{route('empleados.index')}}" class="btn btn-success float-right mr-5" >Listar Empleados</a>
        </div>
        <h2 class=" pb-2 col-10 offset-1 pa-5 "> Consultar Empleado</h2>
        <hr class="pt-2 pb-2 col-10 offset-1 pa-5 ">
    <section class="col-8 offset-2 mb-5">
        <div class="card pa-5">                        
            <div class="card-body ml-4 mt-5 mb-5 pr-5 pl-5">                        
                <h4 class="card-title">Ficha Empleado</h4>    
                <hr>
                @if($empleado->foto != null)  
                <div class="row">
                <img class="img rounded-circle col-sm-3 offset-8" src="{{$empleado->foto}}" style="width:8rem;height:8rem;">
                </div>          
               
                @endif
                <h5 > Id. Empleado: {{$empleado->id_empleado}}</h5>
                <h6>Puesto: {{$empleado->puesto}}</h6>
                
                <p class="card-text">Num. de Licencia: {{$empleado->num_Licencia}}</p>
                <p class="card-text">Num. Seguridad Social: {{$empleado->num_seg_social}} </p>                
                <p class="card-text">Nombre: {{$empleado->nombre}}</p>
                <p class="card-text">Apellidos: {{$empleado->apellidos}}</p>
                <p class="card-text">Fecha Nacimiento: {{$empleado->fecha_nacimiento}}</p>
                <p class="card-text">Fecha Contratación: {{$empleado->fecha_contratacion}}</p>
                <p class="card-text">Teléfono: {{$empleado->telefono}}</p>
                <p class="card-text">Dirección: {{$empleado->direccion}}</p>
                <p class="card-text">Email: {{$empleado->email}}</p>
                <p class="card-text">Situación: {{$empleado->situacion}}</p> 
                <hr>

    </section>
</div>
@endsection