@extends('layouts.app')

@section('content')
    <div class="row">
    
    
    <div class="col-10 offset-1 mb-4 mt-4 pb-2">
        <a href="{{route('empleados.index')}}" class="btn btn-success col-xs-10 offset-xs-1 col-xs-10 offset-xs-1 col-sm-5 offset-sm-6 col-md-2 offset-md-9" >Listar Empleados</a>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Actualizar Empleado </h2>
    <hr class="pt-2 pb-2 col-10 offset-1 pa-5">
    {!! Form::open(array(
            'route'     =>  ['empleados.update',$empleado->id_empleado],
            'method'    =>  'PATCH',
            'class'     =>  'col-10 offset-1 pa-5',
            'files'   =>  'true',
            'validated' => 'true')
        )
    !!}
        
        @include('gestion.empleados.edit')
        
    {!! Form::close() !!}
   </div>    
@endsection 