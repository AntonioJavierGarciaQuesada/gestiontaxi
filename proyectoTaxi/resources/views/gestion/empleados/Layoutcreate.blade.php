@extends('layouts.app')

@section('content')
    
        
        <div class="row">
        <div class="col-10 offset-1 mb-2 mt-4">
            <a href="{{route('empleados.index')}}" class="btn btn-success float-right mr-5" >Listar Empleados</a>
        </div>
        </div>
       
        <h2 class="pb-2 col-10 offset-1 pa-5"> Añadir Empleado</h2>
        <hr>
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>  

        {!! Form::open(array(
                'route'     =>  'empleados.store',
                'method'    =>  'POST',
                'class'     =>  'col-10 offset-1 p-5',
                'files'   =>  'true',
                'validated' => 'true')
            )
        !!}
            @include('gestion.empleados.create')
        
        {!! Form::close() !!}
        
@endsection   