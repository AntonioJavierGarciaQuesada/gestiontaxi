<div class="row mb-3">
    <div class="form-group col">
        <label for="puesto">Puesto</label>
        <select name="puesto" id="puesto" class="custom-select form-control" required>
                <option value="taxista" selected>Taxista</option>
                <option value="controlador">Controlador</option>
        </select>
    </div>
    <div class="form-group col" id="licencia" >
        <label for="num_licencia" class="mr-3">Nº de Licencia</label>
        <input id="num_Licencia" type="text" name="num_Licencia" class="form-control" pattern="^[0-9]{8}[A-Z]{1}$" title="Formato invalida :12345678A">
    </div>
    <div class="form-group  col">
        <label for="num_seg_social" class="mr-3">Nº segurida Social</label>
        <input type="text" name="num_seg_social" class="form-control" pattern="[0-9]{11}" title="Formato invalida :12345678901" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-5">
        <label for="nombre" class="">Nombre</label>
        <input type="text" class="form-control" name="nombre" required>
    </div>
    <div class="form-group col-7">
        <label for="apellidos" class="mr-3">Apellidos</label>
        <input type="text" class="form-control" name="apellidos" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="fecha" class="mr-3">Fecha Nacimiento</label>
        <input type="date"  class="form-control" name="fecha_nacimiento"
        max="<?php echo date('Y-m-d', strtotime('-18 year'));?>" min="1960-01-01" required>
    </div>
    <div class="form-group col">
        <label for="fecha" class="mr-3">Fecha Contratación</label>
        <input type="date"  class="form-control" name="fecha_contratacion"  min="2018-01-01" required>
    </div>
    <div class="form-group col">
        <label for="telefono" class="mr-3">Teléfono</label>
        <input type="text"  class="form-control" name="telefono"  pattern="[6-9]{1}[0-9]{8}$" title="Formato invalida :6-9(12345678)" required>    
    </div>
   
    <div class="form-group col">
        <label for="email">Email</label>
        <input type="email"  class="form-control" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Formato invalida :correo@dominio.com" required>
    </div>

</div>
<div class="row">
<div class="form-group col">
        <label for="direccion">Dirección</label>
        <input type="textarea" class="form-control" name="direccion" required>
    </div>

</div>   
<div class="row">
    <div class="form-group col">
        <label for="foto">Foto </label>
        <input name="foto" type="file" />
    </div>
</div>   
    

    <div class="row justify-content-center">
        <button class="btn btn-primary" id="crearBtn" name="crearBtn" >Crear Empleado</button>
    </div>


    <script>
        //implentar ocultar el número de licencia para los controladores
        window.onload= init;

        function init(){
             var puestoSl = document.getElementById('puesto');
             puestoSl.onchange= manejadorCambiarPuesto;
        }
        function manejadorCambiarPuesto(e){
            var licenciaDiv = document.getElementById('licencia');
            var numLicencia = document.getElementById('num_Licencia');
            if(e.target.value=="taxista"){
                licenciaDiv.setAttribute("style","");
              
            }else{
                licenciaDiv.setAttribute("style","display:none;");

            }
        }

    </script>
