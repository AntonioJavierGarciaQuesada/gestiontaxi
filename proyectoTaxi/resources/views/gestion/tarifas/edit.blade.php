   


    <div class="row mb-3">
        <div class="form-group col" >
            <label for="tipo" class="mr-3">Tipo</label>
            <select name="tipo" class="form-control custom-select" required>
            <option value="Mañana"
                <?php if($tarifa->tipo=="Mañana"){ echo "selected";}?>
            >Mañana</option>
            <option value="Tarde"
                <?php if($tarifa->tipo=="Tarde"){ echo "selected";}?>
            >Tarde</option>
            <option value="Nocturna"
                <?php if($tarifa->tipo=="Nocturna"){ echo "selected";}?>
            >Nocturna</option>
            <option value="Especial"
                <?php if($tarifa->tipo=="Especial"){ echo "selected";}?>
            >Especial</option>             
            </select>
        </div>
        <div class="form-group col">
            <label for="precio" class="mr-3">Precio por Minuto</label>
            <input name="precio_minuto" min="0" step="0.01"  max="3" type="number"  value="{{$tarifa->precio_minuto}}" class="form-control col" placeholder="1.00€" required/> 
        </div>
    </div>   
    <div class="form-group mb-3">
        <label for="descripcion" class="mr-2">Descripción</label>
        <input type="textarea" class="form-control col" value="{{$tarifa->descripcion}}" name="descripcion" required>
    </div>
    
    <div class="row justify-content-center mt-5">
        <button class="btn btn-primary" >Actualizar Tarifa</button>
    </div>