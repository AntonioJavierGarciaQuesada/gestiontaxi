@extends('layouts.app')

@section('content')
    <div class="row mt-5">
    
    
    <div class="col-10 offset-1  pb-2">
        <a href="{{route('tarifas.index')}}" class="btn btn-success float-right mr-5" >Listar Tarifas</a>
    </div>
    <h2 class="pb-2 col-10 offset-1 pa-5"> Actualizar Tarifa</h2>
    <hr class="pt-2 pb-2 col-10 offset-1 pa-5">
    {!! Form::open(array(
            'route'     =>  ['tarifas.update',$tarifa->id_tarifa],
            'method'    =>  'PATCH',
            'class'     =>  'col-10 offset-1 pa-5')
        )
    !!}
        
        @include('gestion.tarifas.edit')
        
    {!! Form::close() !!}
   </div>    
@endsection 