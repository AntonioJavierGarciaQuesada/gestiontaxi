@extends('layouts.app')

@section('content')
    <div class="row mt-5">
    
    <div class="col-10 offset-1">
        <a href="tarifas/create" class="btn btn-success btn-md float-right mr-5" >Crear Tarifas</a>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1"> Listar Tarifas</h2>
    <hr class="pt-2 pb-2 col-10 offset-1" >
    
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr >
                    <th>Id_Tarifa</th>
                    <th>Tipo</th>
                    <th>Descripcion</th>
                    <th>Precio por minuto</th>
                    <th>Acción</th>                          
                </tr> 
                @foreach($tarifas as $tarifa)
                <tr>
                    <td>{{$tarifa->id_tarifa}}</td>
                    <td>{{$tarifa->tipo}}</td>
                    <td>{{$tarifa->descripcion}}</td>
                    <td>{{$tarifa->precio_minuto."€"}}</td>                    
                    <td>
                        <a href="{{route('tarifas.show',$tarifa->id_tarifa)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('tarifas.edit',$tarifa->id_tarifa)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['tarifas.destroy',
                                                $tarifa->id_tarifa                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $tarifas->links() !!}
    </div>    
    </div>
@endsection