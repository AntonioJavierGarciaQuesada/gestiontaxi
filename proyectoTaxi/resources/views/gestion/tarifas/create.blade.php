    <div class="row mb-3">
        <div class="form-group col" >
            <label for="tipo" class="mr-3">Tipo</label>
            <select name="tipo" class="form-control custom-select" required>
                <option selected disabled>Seleccione Tipo Tarifa...</option>
                <option value="Mañana">Mañana</option>
                <option value="Tarde">Tarde</option>
                <option value="Nocturna">Nocturna</option>
                <option value="Especial">Especial</option>            
            </select>
        </div>
        <div class="form-group col">
            <label for="precio" class="mr-3">Precio por Minuto</label>
            <input name="precio_minuto" min="0" step="0.01"  max="3" type="number" class="form-control col" placeholder="1.00€" required/> 
        </div>
    </div>   
    <div class="form-group mb-3">
        <label for="descripcion" class="mr-2">Descripción</label>
        <input type="textarea" class="form-control col" name="descripcion" required>
    </div>
    
    <div class="row justify-content-center mt-5">
        <button class="btn btn-primary" >Crear Tarifa</button>
    </div>