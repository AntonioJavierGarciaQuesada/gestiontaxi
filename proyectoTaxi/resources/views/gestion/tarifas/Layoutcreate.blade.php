@extends('layouts.app')

@section('content')
    
       <div class="row mt-5">
       <div class="col-6 offset-5 ">
            <a href="{{route('tarifas.index')}}" class="btn btn-success btm-md float-right mr-5" >Listar Tarifas</a>
        </div>
        <h2 class=" pb-2 col-10 offset-1 pa-5"> Añadir Tarifa</h2>
        <hr class="pt-2 pb-2 col-10 offset-1 pa-5">
       </div>
       
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>    
        
        {!! Form::open(array(
                'route'     =>  'tarifas.store',
                'method'    =>  'POST',
                'class'     =>  'col-10 offset-1 mt-5')
            )
        !!}
            @include('gestion.tarifas.create')
        
        {!! Form::close() !!}
      
@endsection   