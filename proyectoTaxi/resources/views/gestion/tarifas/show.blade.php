@extends('layouts.app')

@section('content')
<div class="row">
        <h1 class="pt-2 pb-2 col-10 offset-1 pa-5 "> Consultar Tarifa</h1>
        <div class="col-6 offset-5 mb-4 mt-4">
            <a href="{{route('tarifas.index')}}" class="btn btn-success col-xs-10 offset-xs-1 col-sm-5 offset-sm-5 col-md-6 offset-sm-2" >Listar Tarifas</a>
        </div>
    <section class="col-8 offset-2">
        <div class="card">                        
            <div class="card-body">                        
                <h4 class="card-title">Ficha Tarifa</h4>         
                <h5> Id Tarifa: {{$tarifa->id_tarifa}}</h5>
                <p class="card-text">Tipo: {{$tarifa->tipo}} </p>
                <p class="card-text">Descripcion: {{$tarifa->descripcion}}</p>
                <p class="card-text">Precio por Minuto: {{$tarifa->precio_minuto."€"}}</p>
                                        
            </div>
    </section>
</div>
@endsection