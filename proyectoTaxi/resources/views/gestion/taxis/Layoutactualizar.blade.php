@extends('layouts.app')

@section('content')
    <div class="row mt-5">           
            <div class="col-6 offset-5 ">
                <a href="{{route('taxis.index')}}" class="btn btn-success float-right mr-5" >Listar Taxis</a>
            </div>
    </div>  
    <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Añadir Taxi Coche</h2>
    <hr>     
   
    <div class="row">
        <div class="col-xs-12 col-sm-7 mt-5">
            {!! Form::open(array(
                    'route'     =>  ['taxis.update',$taxi->id_taxi],
                    'method'    =>  'PATCH',
                    'class'     =>  'col-10 offset-1 pa-5',
                    'files'   =>  'true',
                    'validated' => 'true')
                )
            !!}
                
                @include('gestion.taxis.edit')
                
            {!! Form::close() !!}
         </div> 
         <div class="col-xs-12 col-sm-4 ml-5 mt-5">
                    @include('gestion.taxis.modelos')           

        </div>
        <script>
                    
            function manejarCheck(e) {
                var evt = e || window.event;
                var target = evt.target||evt.srcElement;
                var codCocheTf = document.getElementById('cod_modelo');
                if(target.checked==true){
                    
                    codCocheTf.value= target.getAttribute('id'); 
                }else{
                    codCocheTf.value= ""; 
                }    
                
            } 
            function init(){
                var checks = document.getElementsByClassName('check'); 
                var tam= checks.length;
                for(var indx = 0; indx<tam; indx++){
                    if(checks[indx].addEventListener){
                        checks[indx].addEventListener('click',manejarCheck,false);
                    }else{
                        checks[indx].attachEvent('onclick',manejarCheck);
                    }

                }
                document.getElementById('crearBtn').onclick=function(){
                        document.getElementById('cod_modelo').removeAttribute('disabled');
                }
               
            }    
            
            window.onload= init;
               
        
        
        </script>    
@endsection 