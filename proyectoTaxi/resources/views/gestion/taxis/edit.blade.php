
    
    <!--- INICIO -->
    <div class="row">
        <div class="form-group col" >
            <label for="num_licencia_taxi">#Licencia Taxi&nbsp;</label>
            <input type="number"  value="{{$taxi->num_licencia_taxi}}" class="form-control" name="num_licencia_taxi" min="10000000" max="99999999" required>    
        </div>
        <div class="form form-group col" >
            <label for="matricula">Matricula</label>
            <input type="text" name="matricula"  value="{{$taxi->matricula}}" class="form-control" placeholder="1234-AAA" pattern="[0-9]{4}-[A-Z]{3}" title="Matrícula Invalida. Formato 1111-AAA" required>    
        </div>
        <div class="form-group col">
            <label for="cod_modelo" class="mr-3">#Modelo</label>
            <input type="number" class="form-control" value="{{$taxi->cod_modelo}}" class="form-control" name="cod_modelo"  id="cod_modelo" disabled="true" required>        
        </div>  
    </div>
    <div class="row">
    <div class="form-group col" >
            <label for="disponibilidad" class="mr-3 col">Disponibilidad</label>
            <select name="disponibilidad" class="custom-select col" id="">
                <option value="0" <?php if($taxi->disponibilidad=="0"){ echo "selected";}?>>Libre</option>
                <option value="1" <?php if($taxi->disponibilidad=="1"){ echo "selected";}?>>Ocupado</option>
            </select>            
    </div>
        <div class="form-group col" >
            <label for="estado" class="mr-3 col">Estado</label>
            <select name="estado" class="custom-select col" id="">
                <option value="0" <?php if($taxi->estado=="0"){ echo "selected";}?>>Activo</option>
                <option value="1" <?php if($taxi->estado=="1"){ echo "selected";}?>>Reparacion</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="year">Fecha Matriculación</label>
            <input type="date" value="{{$taxi->year}}" class="form-control mb-2 mr-2" name="year" min="2005-01-01" max="<?php echo date('Y-m-d');?>" required > 
        </div> 
        <div class="form-group col"> 
            <label for="dataforno">Dispone de Datafono</label>
            <div class="row justify-content-center">
                <label class="radio-inline ml-2 col"><input type="radio"   <?php if($taxi->datafono=="0"){ echo "checked";}?> name="datafono" value="1" required>Si</label>
                <label class="radio-inline ml-1 col"><input type="radio"  <?php if($taxi->datafono=="1"){ echo "checked";}?> name="datafono" value="0">No</label>
            </div>
           
        </div>   
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="propietario" class="mr-3" >Propietario</label>
            <input type="text"  value="{{$taxi->propietario}}" class="form-control col" name="propietario" required>  
        </div>    
    </div>       
    
    <div class="row justify-content-center">
        <button class="btn btn-primary " id="crearBtn" >Actualizar Taxis</button>
    </div>