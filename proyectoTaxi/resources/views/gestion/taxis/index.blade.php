@extends('layouts.app')

@section('content')
    <div class="row mt-5">  
    
    <div class="col-10 offset-1">
        <a href="taxis/create" class="btn btn-success float-right mr-5">Crear Taxi</a>
    </div>
    <h2 class="pb-2 col-10 offset-1"> Listar Taxi</h2>
    <hr class="pt-2 pb-2 col-10 offset-1">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr >
                    <th>Id Taxi</th>
                    <th>Número de Licencia</th>
                    <th>Matricula</th>
                    <th>Estado</th>
                    <th>Disponibilidad</th>
                    <th>Marca</th>
                    <th>Modelo</th> 
                    <th>Acción</th>      
                </tr> 
                @foreach($taxisListado as $taxi)
                <tr>
                    <td>{{$taxi->id_taxi}}</td>
                    <td>{{$taxi->num_licencia_taxi}}</td>
                    <td>{{$taxi->matricula}}</td>
                    <td><?php if($taxi->estado==0){echo "Activo";}else{echo "Reparación";}?></td>
                    <td><?php if($taxi->disponibilidad==0){echo "Libre";}else{echo "Ocupado";}?></td>
                    <td>{{$taxi->marca}}</td>
                    <td>{{$taxi->modelo}}</td>
                    <td>
                        <a href="{{route('taxis.show',$taxi->id_taxi)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('taxis.edit',$taxi->id_taxi)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['taxis.destroy',
                                                $taxi->id_coche                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $taxisListado->links() !!}
    </div>    
    </div>
@endsection