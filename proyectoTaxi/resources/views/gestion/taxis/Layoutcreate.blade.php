@extends('layouts.app')

@section('content')
        <div class="row mt-5">           
            <div class="col-6 offset-5  mt-4">
                <a href="{{route('taxis.index')}}" class="btn btn-success float-right mr-5" >Listar Taxis</a>
            </div>
        </div>  
        <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Añadir Taxi Coche</h2>
        <hr>            
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>  
        <div class="row">
             <div class="col-xs-12 col-sm-6  mt-5">
                {!! Form::open(array(
                        'route'     =>  'taxis.store',
                        'method'    =>  'POST',
                        'class'     =>  'col-10 offset-1',
                        'files'   =>  'true',
                        'validated' => 'true')
                    )
                !!}
                    @include('gestion.taxis.create')
                
                {!! Form::close() !!}
            </div> 
            <div class="col-xs-12  col-sm-12 col-md-6 mt-5">
                    @include('gestion.taxis.modelos')
            </div>

        </div>
        <script>
                    
            function manejarCheck(e) {
                var evt = e || window.event;
                var target = evt.target||evt.srcElement;
                var codCocheTf = document.getElementById('cod_modelo');
                if(target.checked==true){
                    
                    codCocheTf.value= target.getAttribute('id'); 
                }else{
                    codCocheTf.value= ""; 
                }    
                
            } 
            function init(){
                var checks = document.getElementsByClassName('check'); 
                var tam= checks.length;
                for(var indx = 0; indx<tam; indx++){
                    if(checks[indx].addEventListener){
                        checks[indx].addEventListener('click',manejarCheck,false);
                    }else{
                        checks[indx].attachEvent('onclick',manejarCheck);
                    }

                }
                document.getElementById('crearBtn').onclick=function(){
                        document.getElementById('cod_modelo').removeAttribute('disabled');
                }
               
            }    
            
            window.onload= init;
               
        
        
        </script> 
@endsection   