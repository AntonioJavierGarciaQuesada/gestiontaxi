      
    <div class="row">
        <div class="form-group col" >
            <label for="num_licencia_taxi">#Licencia Taxi&nbsp;</label>
            <input type="number" class="form-control" name="num_licencia_taxi" min="10000000" max="99999999"  required>    
        </div>
        <div class="form form-group col" >
            <label for="matricula">Matricula</label>
            <input type="text" name="matricula"  class="form-control" placeholder="1234-AAA" pattern="[0-9]{4}-[A-Z]{3}" title="Matrícula Invalida. Formato 1111-AAA" required>    
        </div>
        <div class="form-group col">
            <label for="cod_modelo" class="mr-3">#Modelo</label>
            <input type="number" class="form-control"  class="form-control" name="cod_modelo"  id="cod_modelo" disabled="true" required>        
        </div>  
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="year">Fecha Matriculación</label>
            <input type="date" class="form-control mb-2 mr-2" name="year" min="2005-01-01" max="<?php echo date('Y-m-d');?>" required > 
        </div> 
        <div class="form-group col"> 
            <label for="dataforno">Dispone de Datafono</label>
            <div class="row justify-content-center">
                <label class="radio-inline ml-2 col"><input type="radio" name="datafono" value="1" required>Si</label>
                <label class="radio-inline ml-1 col"><input type="radio" name="datafono" value="0">No</label>
            </div>
           
        </div>   
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="propietario" class="mr-3" >Propietario</label>
            <input type="text"  class="form-control col" name="propietario" required>  
        </div>    
    </div>       
    
    <div class="row justify-content-center">
        <button class="btn btn-primary " id="crearBtn" >Crear Taxis</button>
    </div>