@extends('layouts.app')

@section('content')
<div class="row mt-5">
        
        <div class="col-6 offset-5 mt-4">
            <a href="{{route('taxis.index')}}" class="btn btn-success float-right mr-5" >Listar Taxis</a>
        </div>
</div> 
<h2 class="pt-2 pb-2 col-10 offset-1"> Consultar Taxi</h2>
<hr>       
    <section class="row mb-4">
        
        <div class="card col-xs-12 col-sm-5 offset-sm-1 pa-5">                        
            <div class="card-body mt-2">
                                        
                <h4 class="card-title">Datos taxi</h4>           
               <hr>
                    <h5> Id taxi: {{$taxi->id_taxi}}</h5>
                    <hr>
                    <p class="card-text">Número Licencia Taxi: {{$taxi->num_licencia_taxi}} </p>
                    <p class="card-text">Matricula:{{$taxi->matricula}}</p>
                    <p class="card-text">Estado: {{$taxi->estado}}</p>
                    <p class="card-text">Disponibilidad:<?php if($taxi->disponibilidad==0){echo "LIBRE";}else{echo "OCUPADO";}?>/p> 
                    <p class="card-text">Datafono :<?php if($taxi->datafono==0){echo "No disponible";}else{echo "Disponible";}?>/p>   
                    <p class="card-text">Fecha de Matriculación: {{$taxi->year}}</p>
                    <p class="card-text">Propietario: {{$taxi->propietario}}</p> 
                <hr>           
                       
               </div>                                              
            </div>
            <div class="card col-xs-12 col-sm-5 ml-5 pa-5">                        
                <div class="card-body mt-2">                        
                    <h4 class="card-title">Datos Modelo</h4> 
                    <hr>              
                    <h5> Cod modelo: {{$taxi->cod_modelo}}</h5>
                    <hr>
                    <p class="card-text">Marca: {{$taxi->marca}} </p>
                    <p class="card-text">Modelo:{{$taxi->modelo}}</p>
                    <p class="card-text">Descripcion: {{$taxi->descripcion}}</p>
                    <p class="card-text">Número Plazas:{{$taxi->num_plazas}}</p>           
                       
               </div> 
            </div>                                                
            
        </div>

             

    </section>
</div>
@endsection