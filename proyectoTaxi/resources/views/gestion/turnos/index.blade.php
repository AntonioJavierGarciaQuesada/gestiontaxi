@extends('layouts.app')

@section('content')
    <div class="row mt-4">   

        <div class="col-10 offset-1 mb-4 mt-4 pb-2">
            <a href="turnos/show" class="btn btn-info">Consultar Turnos Semana</a>
            @if(Auth::user()!==null && Auth::user()->hasRole('admin')) 
            <a href="turnos/create" class="btn btn-success float-right mr-5" >Crear Turno</a>
            @endif
        </div>
    </div>
    <div class="row">   
        <h2 class="pt-2 pb-2 col-10 offset-1"> Listar Turnos</h2>
        <hr class="pt-2 pb-2 col-10 offset-1">
    </div>
    <div class="row">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    </div>
    <div class="row">    
   
    <div class="col-10 offset-1">
        
        <table class="table table-striped">        
                <tr >
                    <th>Id Turno</th>
                    <th>Fecha Inicio</th>
                    <th>Hora Inicio</th>
                    <th>Hora Inicio Real</th>
                    <th>Fecha Fin</th>
                    <th>Hora Fin</th>
                    <th>Hora Fin Real</th> 
                    <th>Nombre</th>
                    <th>Matricula </th>
                    @if(Auth::user()!==null && Auth::user()->hasRole('admin'))
                    <th>Acción</th> 
                    @endif     
                </tr> 
                @foreach($turnosListado as $turno)
                <tr>
                    <td>{{$turno->id_turno}}</td>
                    <td>{{$turno->fecha_inicio}}</td>
                    <td>{{$turno->hora_inicio}}</td>
                    <td>{{$turno->hora_inicio_real}}</td>
                    <td>{{$turno->fecha_fin}}</td>
                    <td>{{$turno->hora_fin}}</td>
                    <td>{{$turno->hora_fin_real}}</td>
                    <td>{{$turno->nombre.",".$turno->apellidos}}</td>
                    <td>{{$turno->matricula}}</td>
                    @if(Auth::user()!==null && Auth::user()->hasRole('admin'))
                    <td>                      
                        <a href="{{route('turnos.edit',$turno->id_turno)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['turnos.destroy',
                                                $turno->id_turno                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    @endif
                    
                </tr>
                @endforeach        
        </table>
    </div>
    
    <div class="col-8 offset-2">
        {!! $turnosListado->links() !!}
    </div>    
    </div>
@endsection