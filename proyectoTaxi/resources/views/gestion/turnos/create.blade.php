      
    
    <div class="row">
        <div class="form-group col">
            <label for="fecha_incio" class="mr-3">Fecha Inicio</label>
            <input type="date" min="<?php echo date('Y-m-d');?>" name="fecha_inicio" class="form-control" required/>            
        </div>    
        <div class="form-group col" >    
            <label for="hora_inicio" class="mr-3">Hora Inicio</label>
            <input type="time" name="hora_inicio" id="fecha_inicio" class="form-control" required/>                
        </div>
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="fecha_fin" class="mr-3">Fecha Fin</label>
            <input type="date" name="fecha_fin" class="form-control" id="fecha_fin" min="<?php echo date('Y-m-d');?>" required/>
        </div>
        <div class="form-group col">    
            <label for="hora_fin" class="mr-3">Hora Fin</label>
            <input type="time" name="hora_fin" class="form-control" required/>    
        </div>
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="modelo" class="mr-3">Código Taxista</label>
            <input type="number" class="form-control col-sm-12" name="cod_taxista"  id="cod_taxista" disabled="true" required>        
        </div> 
        <div class="form-group col">
            <label for="modelo" class="mr-3">Código Taxi</label>
            <input type="number" class="form-control col-sm-12" name="cod_taxi"  id="cod_taxi" disabled="true" required>        
        </div>   
    </div>    
    
    <div class="row justify-content-center" class="mr-3">
        <button class="btn btn-primary" id="crearBtn" >Crear Turno</button>
    </div>