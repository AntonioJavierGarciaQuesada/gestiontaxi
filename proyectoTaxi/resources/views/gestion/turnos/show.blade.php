@extends('layouts.app')

@section('content')
    <div class="row mt-5">
   
    
    <div class="col-10 offset-1 pb-2">       
        <a href="{{route('turnos.index')}}" class="btn btn-success float-right mr-5" >Listar Turnos</a>
    </div>
    </div>
    <div class="row">
        <h2 class="pt-2 pb-2 col-10 offset-1"> Consultar Turno Semanal</h2>
        <hr class="col-10 offset-1">
    </div>
    <div class="col-10 offset-1 mt-3">
        <table class="table table-striped">        
                <tr >
                    <th>Id Turno</th>
                    <th>Fecha Inicio</th>
                    <th>Hora Inicio</th>
                    <th>Hora Inicio Real</th>
                    <th>Fecha Fin</th>
                    <th>Hora Fin</th>
                    <th>Hora Fin Real</th> 
                    <th>Nombre</th>
                    <th>Matricula </th>
                    <th>Acción</th>      
                </tr> 
                @foreach($turnosListado as $turno)
                <tr>
                    <td>{{$turno->id_turno}}</td>
                    <td>{{$turno->fecha_inicio}}</td>
                    <td>{{$turno->hora_inicio}}</td>
                    <td>{{$turno->hora_inicio_real}}</td>
                    <td>{{$turno->fecha_fin}}</td>
                    <td>{{$turno->hora_fin}}</td>
                    <td>{{$turno->hora_fin_real}}</td>
                    <td>{{$turno->nombre.",".$turno->apellidos}}</td>
                    <td>{{$turno->matricula}}</td>
                    <td>                      
                        <a href="{{route('turnos.edit',$turno->id_turno)}}" class="btn btn-primary">Actualizar</a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['turnos.destroy',
                                                $turno->id_turno                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        {!! Form::submit('Delete',['class'=>'btn btn-danger'])!!}
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $turnosListado->links() !!}
    </div>    
    </div>
@endsection