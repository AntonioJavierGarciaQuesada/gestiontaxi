@extends('layouts.app')

@section('content')
    
    
        <div class="row mt-4">        
            
            <div class="col-6 offset-5 mt-4">
                <a href="{{route('turnos.index')}}" class="btn btn-success float-right mr-5" >Listar Turnos</a>
            </div>
        </div>
        <h2 class=" pb-2 col-10 offset-1"> Añadir Turno</h2>
        <hr>
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>      
        <div class="row mt-4 mb-5">
             <div class="col-sm-6">
                {!! Form::open(array(
                        'route'     =>  'turnos.store',
                        'method'    =>  'POST',
                        'class'     =>  'col-10 offset-1',
                        'files'   =>  'true')
                    )
                !!}
                    @include('gestion.turnos.create')
                
                {!! Form::close() !!}
            </div> 
            <div class="col-md-5 ml-5 ">
            <div class="form-inline form-group">
                    <label for="" class="mr-3">Seleccione Tabla</label>
                    <select name="tabla" class="form-control custom-select" id="visualizar">
                        <option value="taxistas">Taxistas</option>
                        <option value="taxis">Taxis</option>
                    </select>
                </div>
                <div id="taxis" style="display:none">
                        @include('gestion.turnos.taxis')
                </div>
                <div id="taxistas" class="div-tabla">
                    @include('gestion.turnos.taxistas')                   
                </div>        
            </div>
        </div>    
                    
                
        <script>
                    
                    function manejarCheck(e) {
                        var evt = e || window.e;
                        var target = evt.target||evt.srcElement;
                        var codTaxistaTf = document.getElementById('cod_taxista');               
                        var codTaxiTf = document.getElementById('cod_taxi');                         
                        var tabla =target.parentElement.parentElement.parentElement.parentElement;
                        var tabla = tabla.getAttribute('id');
                        
                        if(target.checked==true){
                            if(tabla=="tablaTaxis") {
                                codTaxiTf.value= target.getAttribute('id');
                            }else{
                                codTaxistaTf.value= target.getAttribute('id');
                            }                                 
                                        
                        }else{
                            if(tabla=="tablaTaxis"){
                                codTaxiTf.value= "";
                            }else{
                                codTaxistaTf.value= "";
                            }    
                                
                        }    
                        
                    } 
        
                    function manejadorVisualizar(e){
                        
                        var evt = e || window.event;
                        var target = evt.target||evt.srcElement;
                        var divSelec = document.getElementsByClassName("div-tabla")[0];
                        divSelec.removeAttribute("class");
                        divSelec.setAttribute("style","display:none;")
                        if(target.value == "taxis"){
                            var cambiarDiv=document.getElementById("taxis"); 
                        }else{
                            var cambiarDiv=document.getElementById("taxistas");      
                        }                     
                        
                        cambiarDiv.setAttribute("class","div-tabla");
                        cambiarDiv.removeAttribute("style"); 
                    }

                    function init(){
                        var checks = document.getElementsByClassName('check'); 
                        var select = document.getElementById('visualizar');
                        if(select.addEventListener){
                            select.addEventListener("change",manejadorVisualizar,false);
                        }else{
                            select.attchEvent("onchage",manejadorVisualizar);
                        }
                        
                        var tam= checks.length;
                        for(var indx = 0; indx<tam; indx++){
                            if(checks[indx].addEventListener){
                                checks[indx].addEventListener('click',manejarCheck,false);
                            }else{
                                checks[indx].attachEvent('onclick',manejarCheck);
                            }
        
                        }

                        document.getElementById('crearBtn').onclick=function(){
                                
                                document.getElementById('cod_taxi').removeAttribute('disabled');
                                document.getElementById('cod_taxista').removeAttribute('disabled');
                        }
                       
                    }    
                    
                    window.onload= init;
                       
                
                
                </script> 
         
@endsection   