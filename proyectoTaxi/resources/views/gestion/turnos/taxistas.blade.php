
    <table class="table table-striped" style="max-height:300px overflow: scroll" id="tablaTaxistas">        
                <tr >
                    <th></th>
                    <th>Nombre</th>
                    <th>#Licencia</th>
                    <th>Teléfono</th>
                </tr> 
                @foreach($taxistas as $taxista)
                <tr>
                    <td><input type="radio" class="check" name="taxista" id="{{$taxista->id_empleado}}"></td>
                    <td>{{$taxista->nombre.", ".$taxista->apellidos}}</td>
                    <td>{{$taxista->num_Licencia}}</td>
                    <td>{{$taxista->telefono}}</td>                   
                </tr>
                @endforeach        
    </table>