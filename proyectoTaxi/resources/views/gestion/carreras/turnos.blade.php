<table class="table table-striped" style="max-height:300px overflow: scroll" id="tablaTurnos">        
    <tr >
        <th></th>
        <th>Fecha Ini</th>                    
        <th>Fecha Fin</th>
        <th>ID Taxi</th>
        <th>ID Empleado</th>              
    </tr>                 
    @foreach($turnos as $turno)
    <tr>
    <td><input type="radio" class="check" name="turno" id="{{$turno->id_turno}}"></td>
        <td>{{$turno->fecha_inicio." ".$turno->hora_inicio}}</td>
        <td>{{$turno->fecha_fin." ".$turno->hora_fin}}</td>
        <td>{{$turno->cod_taxi}}</td> 
        <td>{{$turno->cod_taxista}}</td>        
    </tr>
    @endforeach        
</table>