@extends('layouts.app')

@section('content')
    <div class="row mt-5">
   
    
    <div class="col-10 offset-1 ">        
        <a href="carreras/create" class="btn btn-success float-right mr-5">Crear Carrera</a>
    </div>
    <h2 class="col-10 offset-1"> Listar Carreras</h2>
    <hr class="pt-2 pb-2 col-10 offset-1">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table  table-striped">        
      
            <tr>
                    <th>Id Carrera</th>                   
                    <th>Tarifa</th>
                    <th>Turno</th>
                    <th>Controlador</th>
                    <th>Origen</th>
                    <th>Destino</th> 
                    <th>Zona</th>                  
                    <th>Acción</th>      
                </tr>
       
        <tbody         
                @foreach($carreras as $carrera)
                <tr>
                    <td>{{$carrera->id_carrera}}</td>                    
                    <td>{{$carrera->cod_tarifa}}</td>
                    <td>{{$carrera->cod_turno}}</td>
                    <td>{{$carrera->cod_controlador}}</td>
                    <td>{{$carrera->origen}}</td>
                    <td>{{$carrera->destino}}</td> 
                    <td>{{$carrera->zona}}</td>                   
                    <td>
                        <a href="{{route('carreras.show',$carrera->id_carrera)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>                      
                        <a href="{{route('carreras.edit',$carrera->id_carrera)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['carreras.destroy',
                                                $carrera->id_carrera                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach 
            </tbody>           
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $carreras->links() !!}
    </div>    
    </div>
@endsection