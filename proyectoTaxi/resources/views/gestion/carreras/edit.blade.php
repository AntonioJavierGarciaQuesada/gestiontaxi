      
    <div class="row">
        <div class="col-sm-6 mb-5">
            <div class="row">
                <div class="form-group col">
                    <label for="modelo">Cod. Tarifa</label>
                    <input type="number" class="form-control col" value="{{$carrera->cod_tarifa}}" name="cod_tarifa"  id="cod_factura" disabled="true" required>        
                </div> 
                <div class="form-group col">
                    <label for="modelo">Cod. Turno</label>
                    <input type="number" class="form-control col"  value="{{$carrera->cod_turno}}" name="cod_turno"  id="cod_turno" disabled="true" required>        
                </div>
                
                <div class="form-group col">
                    <label for="modelo">Cod. Controlador</label>
                    <input type="number" value="{{$carrera->id_empleado}}" class="form-control number col" name="cod_controlador"  id="id_empleado" disabled="true" >        
                </div>    
            </div> 
            <div class="row">
                <div class="form-group col" >
                    <label for="duracion">Duración </label>
                    <input type="time" class="form-control"  value="{{$carrera->duracion}}" name="duracion" required>    
                </div>
                
                <div class="form-group col">
                    <label for="estado">Tipo Cliente</label>
                    <select name="tipo_cliente" class="custom-select col">                              
                        <option value="cliente_llamada"  <?php if($carrera->tipo_cliente=="cliente_llamada"){echo " selected";}?>>Teléfono</option>
                        <option value="cliente_aplicacion"  <?php if($carrera->tipo_cliente=="cliente_aplicacion"){echo " selected";}?>>Internet</option>
                        <option value="cliente_calle"  <?php if($carrera->tipo_cliente=="cliente_calle"){echo " selected";}?>>Pie de Calle</option> 
                        <option value="otros"  <?php if($carrera->tipo_cliente=="cliente_otros"){echo " selected";}?>>Otros</option>           
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="precio">Precio</label>
                    <input type="number" value="<?php if(!empty($carrera->precio)){echo $carrera->precio;}?>" step="0.01" name="precio" min="0" class="form-control number col" placeholder="" required>    
                </div>
                <div class="form-group col">
                    <label for="tipo_pago">Forma de Pago</label>
                    <select name="tipo_pago" class="custom-select col">
                        <option value="Contado"  <?php if($carrera->tipo_pago=="Contado"){echo " selected";}?>>Contado</option>
                        <option value="Tarjeta"  <?php if($carrera->tipo_pago=="Tarjeta"){echo " selected";}?>>Tarjeta de Credito</option>
                                
                    </select>
                </div>
            </div> 
            <div class="row">
                <div class="form-group col">
                    <label for="origen">Origen</label>
                    <input type="text"  value="{{$carrera->origen}}" class="form-control col" name="origen" placeholder="" required >        
                </div>                
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="destino">Destino</label>
                    <input type="text"  value="{{$carrera->destino}}" class="form-control col" name="destino" required>        
                </div>
            </div>        
            <div class="row">
                <div class="form-group col">
                    <label for="disponiblidad">Posicion gps Origen</label>
                    <input type="text" value="{{$carrera->gps_origen}}"  class="form-control col" name="gps_origen" required>        
                </div>
                <div class="form-group col">
                    <label for="disponiblidad">Posicion gps Destino</label>
                    <input type="text" value="{{$carrera->gps_destino}}" class="form-control col" name="gps_destino" required>        
                </div>
            </div>      
            <div class="row">
                <div class="form-group col">
                    <label for="zona">Zona</label>
                    <select name="zona" class="custom-select col">
                        <option value="zona1" <?php if($carrera->tipo_zona=="zona1"){echo " selected";}?>>Zona 1</option>
                        <option value="zona2" <?php if($carrera->tipo_zona=="zona2"){echo " selected";}?>>Zona 2</option>
                        <option value="zona3" <?php if($carrera->tipo_zona=="zona3"){echo " selected";}?>>Zona 3</option>
                        <option value="zona4" <?php if($carrera->tipo_zona=="zona4"){echo " selected";}?> >Zona 4</option>
                        <option value="zona5" <?php if($carrera->tipo_zona=="zona5"){echo " selected";}?>>Zona 5</option>
                    </select>        
                </div>
            </div>
            
            <div class="form-group col-12">
                <button class="btn btn-primary col-xs-10 offset-xs-1 col-sm-4 offset-sm-4 " id="crearBtn" >Actualizar Carrera</button>
            </div>
         </div>
    <div class="col-sm-5 offset-sm-1 mb-5">
        <div class="form-group">
            <label for="visualizar">Visualiazar la tabla</label>
            <select id="visualizar" class="custom-select">                   
                <option value="tarifa">Tarifa</option>
                <option value="turno">Turno</option>
                <option value="controlador">Controlador</option>    
            </select>
         </div> 
        <div id="turnos" style="display:none" >
            <?php $turnos=$datos[2];?> 
            @include('gestion.carreras.turnos')              
        </div>    
        <div id="tarifas"  class="div-tabla">
            <?php $tarifas=$datos[1];?>
            @include('gestion.carreras.tarifas')
        </div> 
        <div id="controladores" style="display:none">
            <?php $controladores=$datos[0];?>
            @include('gestion.carreras.controladores')
        </div> 
    </div> 


 