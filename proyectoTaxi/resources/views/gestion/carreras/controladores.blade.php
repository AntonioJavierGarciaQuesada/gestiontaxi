<table class="table table-striped" style="max-height:300px overflow: scroll" id="tablaControladores">        
        <tr >
            <th></th>
            <th>Nombre</th>                    
            <th>Puesto</th>
            <th>Teléfono</th>                                  
                    
        </tr>                 
        @foreach($controladores as $controlador)
        <tr>
            <td><input type="radio" class="check" name="controlador" id="{{$controlador->id_empleado}}"></td>
            <td>{{$controlador->nombre.", ".$controlador->apellidos}}</td>
            <td>{{$controlador->puesto}}</td>
            <td>{{$controlador->telefono}}</td>
            
        </tr>
        @endforeach        
</table>