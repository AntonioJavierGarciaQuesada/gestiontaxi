@extends('layouts.app')

@section('content')
        <div class="row mt-5">
            
            
            <div class="col-6 offset-5">
                <a href="{{route('carreras.index')}}" class="btn btn-success float-right mr-5" >Listar carreras</a>
            </div>
            <h2 class=" col-10 offset-1 "> Actualizar Carrera</h2>
            <hr class="pt-2 col-10 offset-1 ">
        </div>

        <div class="row">
             
             {!! Form::open(array(
                'route'     =>  ['carreras.update',$carrera->id_carrera],
                'method'    =>  'PATCH',
                'class'     =>  'col-10 offset-1 pa-5',
                'Validated')
            )
        !!}
                    @include('gestion.carreras.edit')
                
                {!! Form::close() !!}
           
            

        </div>
        <script>
                    
            function manejarCheck(e) {
                var evt = e || window.e;
                var target = evt.target||evt.srcElement;
                var codTurnoTf = document.getElementById('cod_turno');               
                var codFacturaTf = document.getElementById('cod_factura');
                var codControladorTf = document.getElementById('id_empleado'); 
                var tabla =target.parentElement.parentElement.parentElement.parentElement;
                var tabla = tabla.getAttribute('id');
                
                if(target.checked==true){
                    switch (tabla) {
                        case "tablaTurnos":
                            codTurnoTf.value= target.getAttribute('id');
                        break;
                        case "tablaTarifas":
                            codFacturaTf.value= target.getAttribute('id');
                            break;
                        case "tablaControladores":
                            codControladorTf.value= target.getAttribute('id');
                            break;    
                    }
                                
                }else{
                    switch (tabla) {
                        
                        case "tablaTurnos":
                            codTurnoTf.value= "";
                        default:
                        case "tablaFacturas":
                            codFacturaTf.value= "";
                            break;
                        case "tablaFacturas":
                            codControladorTf.value= "";
                            break;    
                    }
                }    
                
            } 

            function manejadorVisualizar(e){
                
                var evt = e || window.event;
                var target = evt.target||evt.srcElement;
                var divSelec = document.getElementsByClassName("div-tabla")[0];
                divSelec.removeAttribute("class");
                divSelec.setAttribute("style","display:none;")
                switch (target.value) {
                    case "turno":
                        var cambiarDiv=document.getElementById("turnos");             
                        break;
                    case "tarifa":
                        var cambiarDiv=document.getElementById("tarifas");             
                        break;
                    case "controlador":
                        var cambiarDiv=document.getElementById("controladores");             
                        break;
                           
                
                    default:
                        break;
                }
                       cambiarDiv.setAttribute("class","div-tabla");
                        cambiarDiv.removeAttribute("style"); 
            }
            function init(){
                var checks = document.getElementsByClassName('check'); 
                var select = document.getElementById('visualizar');
                if(select.addEventListener){
                    select.addEventListener("change",manejadorVisualizar,false);
                }else{
                    select.attchEvent("onchage",manejadorVisualizar);
                }
                
                var tam= checks.length;
                for(var indx = 0; indx<tam; indx++){
                    if(checks[indx].addEventListener){
                        checks[indx].addEventListener('click',manejarCheck,false);
                    }else{
                        checks[indx].attachEvent('onclick',manejarCheck);
                    }

                }
                document.getElementById('crearBtn').onclick=function(){
                        
                        document.getElementById('id_empleado').removeAttribute('disabled');
                        document.getElementById('cod_factura').removeAttribute('disabled');
                        document.getElementById('cod_turno').removeAttribute('disabled');
                }
               
            }    
            
            window.onload= init;
               
        
        
        </script> 
@endsection   