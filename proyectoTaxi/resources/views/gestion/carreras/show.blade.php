@extends('layouts.app')

@section('content')
<div class="row mt-5">
        
        <div class="col-6 offset-5 ">
            <a href="{{route('carreras.index')}}" class="btn btn-success float-right" >Listar Carreras</a>
        </div>
        <h2 class="pb-2 col-10 offset-1 pa-5"> Consultar Carrera</h2>
        <hr class="pt-2 pb-2 col-10 offset-1 pa-5"> 
</div>        
    <section class="row mb-4">
        
        <div class="card col-10 offset-1 pa-4">                        
            <div class="card-body mb-2">                        
                <h4 class="card-title mt-3">Datos Carrera</h4>   
                <hr>             
               
                    <h5> Id Carrera: {{$carrera->id_carrera}}</h5>
                    <hr>
                    <p class="card-text">
                        <?php                     
                            if(isset($carrera->duracion)){echo "Duración: ".$carrera->duracion;}
                            if(isset($carrera->precio)){echo " Precio: ".$carrera->precio."€";}
                        ?>
                    </p>
                    <p class="card-text">
                        <?php                     
                            if(isset($carrera->origen)){echo "Origen: ".$carrera->origen;}
                            if(isset($carrera->destino)){echo " Destino: ".$carrera->destino;}
                        ?>
                    <p> 
                    <p class="card-text">
                        <?php                     
                            echo "Posición Gps Origen:";
                            if(isset($carrera->gps_origen)){echo $carrera->gps_origen;}
                            echo " Destino: ";
                            if(isset($carrera->gps_destino)){echo $carrera->gps_destino;}
                        ?>
                    <p>
                    <p class="card-text"><?php                     
                            if(isset($carrera->tipo_cliente)){echo "Tipo Cliente: ".$carrera->tipo_cliente;}
                            if(isset($carrera->tipo_pago)){echo " Forma Pago: ".$carrera->tipo_pago;}
                        ?></p> 
                        <hr>                  
                                           
               </div>                                              
            </div>                                                          
            
        </div>

             

    </section>
</div>
@endsection