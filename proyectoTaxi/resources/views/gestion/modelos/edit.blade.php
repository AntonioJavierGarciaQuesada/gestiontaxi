    <div class="row">
        <div class="form-group col" >
            <label for="marca">Marca</label>
            <select name="marca" class="form-control custom-select" required>
            <option value="Seat"
        <?php if($modelo->marca=="Seat"){ echo "selected";}?>
        >Seat</option>
        <option value="Ford"
        <?php if($modelo->marca=="Ford"){ echo "selected";}?>
        >Ford</option>
        <option value="Toyota"
        <?php if($modelo->marca=="Toyota"){ echo "selected";}?>
        >Toyota</option>
        <option value="Skoda"
        <?php if($modelo->marca=="Skoda"){ echo "selected";}?>
        >Skoda</option>
        <option value="Mercedes"
        <?php if($modelo->marca=="Mercedes"){ echo "selected";}?>
        >Mercedes</option>
        <option value="Volkswagen"
        <?php if($modelo->marca=="Volkswagen"){ echo "selected";}?>
        >Volkswagen</option>
        <option value="Opel"
        <?php if($modelo->marca=="Opel"){ echo "selected";}?>
        >Opel</option>
        <option value="Reanault"
        <?php if($modelo->marca=="Reanault"){ echo "selected";}?>
        >Renault</option>
        <option value="Citroen"
        <?php if($modelo->marca=="Citroen"){ echo "selected";}?>
        >Citroen</option>
        <option value="Audi"
        <?php if($modelo->marca=="Audi"){ echo "selected";}?>
        >Audi</option>
        <option value="Volvo"
        <?php if($modelo->marca=="Volvo"){ echo "selected";}?>
        >Volvo</option>     
            </select>
        </div>
        <div class="form-group col">
            <label for="numPlazas">Número Plazas</label>
            <input type="number" value="{{$modelo->num_plazas}}" value="3" class="form-control ml-2" min=3 max=6 name="num_plazas" required>
        </div> 
        <div class="form-group col">
            <label for="cilindrada" class="">Cilindrada</label>
            <select name="cilindrada" class="form-control custom-select" required>
            <option value="1.4" 
        <?php if($modelo->cilindrada=="1.4"){ echo "selected";}?>
            >1400</option>
        <option value="1.6"
         <?php if($modelo->cilindrada=="1.6"){ echo "selected";}?>
        >1600</option>
        <option value="1.8"
        <?php if($modelo->cilindrada=="1.8"){ echo "selected";}?>
        >1800</option>
        <option value="1.9" 
        <?php if($modelo->cilindrada=="1.9"){ echo "selected";}?>
        >1900</option>
        <option value="2.0"
        <?php if($modelo->cilindrada=="2.0"){ echo "selected";}?>
        >2000</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="modelo">Modelo</label>
            <input type="text" class="form-control" value="{{$modelo->modelo}}" name="modelo" required>
        </div>        
        <div class="form-group col">
            <label for="descripcion">Descripción</label>
            <input type="text" class="form-control " name="descripcion" value="{{$modelo->descripcion}}" required>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label for="foto">Foto </label>
            <input name="foto" type="file" /> 
        </div>
    </div>
    
    <div class="row justify-content-center">
         <button class="btn btn-primary" >Actualizar Modelo</button>
   </div>