@extends('layouts.app')

@section('content')
<div class="row mt-5">
        
        <div class="col-6 offset-5 mt-4">
            <a href="{{route('modelos.index')}}" class="btn btn-success float-right mr-5" >Listar Modelos</a>
        </div>
        <h2 class=" pb-2 col-10 offset-1 pa-5 "> Consultar Modelo Coche</h2>
        <hr class="pt-2 pb-2 col-10 offset-1 pa-5 "> 
    <section class="col-8 offset-2">
        <div class="card p-5">                        
            <div class="card-body">                        
                <h4 class="card-title">Ficha Modelo Coche</h4>
                <hr>
                <div class="row mb-3">
                    <img class="card-img-top img-fluid" src="{{$modelo->foto}}" alt="5rem">
                </div>
                
                <h5> Id modelo: {{$modelo->id_coche}}</h5>
                <p class="card-text">Marca: {{$modelo->marca}} </p>
                <p class="card-text">Modelo:{{$modelo->modelo}}</p>
                <p class="card-text">Descripcion: {{$modelo->descripcion}}</p>
                <p class="card-text">Número Plazas:{{$modelo->num_plazas}}</p>  
                <hr>                      
            </div>
    </section>
</div>
@endsection