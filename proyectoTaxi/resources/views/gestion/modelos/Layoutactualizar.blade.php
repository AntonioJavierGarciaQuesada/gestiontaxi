@extends('layouts.app')

@section('content')
    <div class="row mt-5">
    
    
    <div class="col-10 offset-1">
        <a href="{{route('modelos.index')}}" class="btn btn-success float-right mr-5" >Listar Modelos</a>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Actualizar Modelo Coche</h2>
   <hr class="pt-2 pb-2 col-10 offset-1 pa-5">
    {!! Form::open(array(
            'route'     =>  ['modelos.update',$modelo->id_coche],
            'method'    =>  'PATCH',
            'class'     =>  'col-10 offset-1 pa-5',
            'files'   =>  'true',
            'validated' => 'true')
        )
    !!}
        
        @include('gestion.modelos.edit')
        
    {!! Form::close() !!}
   </div>    
@endsection 