@extends('layouts.app')

@section('content')
    <div class="row mt-5">    
    
    <div class="col-10 offset-1">
        <a href="modelos/create" class="btn btn-success float-right mr-5" >Crear Modelos</a>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1"> Listar Modelos Coches</h2>
    <hr <h1 class="pt-2 pb-2 col-10 offset-1">
    @if($message = Session::get('success'))
        <div class="alert alert-success col-10 offset-1">
            <p class="pt-2"> {{$message}}</p>
        </div>
    @endif
    <div class="col-10 offset-1">
        <table class="table table-striped">        
                <tr >
                    <th>Id modelo</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Número de Plazas</th>
                    <th>Cilindrada</th>
                    <th>Descripción</th>
                    <th>Acción</th>        
                </tr> 
                @foreach($modelos as $modelo)
                <tr>
                    <td>{{$modelo->id_coche}}</td>
                    <td>{{$modelo->marca}}</td>
                    <td>{{$modelo->modelo}}</td>
                    <td>{{$modelo->num_plazas}}</td>
                    <td>{{$modelo->cilindrada}}</td>
                    <td>{{$modelo->descripcion}}</td>
                    <td>
                        <a href="{{route('modelos.show',$modelo->id_coche)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('modelos.edit',$modelo->id_coche)}}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        {!! Form::open([
                                    'method' => 'delete',
                                    'route' => ['modelos.destroy',
                                                $modelo->id_coche                       
                                                ],
                                    'style'=>'display:inline'])
                        !!}
                        <button type="submit" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    </td>
                    
                </tr>
                @endforeach        
        </table>
    </div>
    <div class="col-8 offset-2">
        {!! $modelos->links() !!}
    </div>    
    </div>
@endsection