@extends('layouts.app')

@section('content')
    <div class="row mt-4">
    <div class="col-6 offset-5  mt-4">
            <a href="{{route('modelos.index')}}" class="btn btn-success btn-md float-right mr-5" >Listar Modelos</a>
        </div>
    </div>
    <h2 class="pt-2 pb-2 col-10 offset-1 pa-5"> Añadir Modelo Coche</h2>
    <hr>   
        
        
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>  

        {!! Form::open(array(
                'route'     =>  'modelos.store',
                'method'    =>  'POST',
                'class'     =>  'col-10 offset-1 p-5',
                'files'   =>  'true',
                'validated' => 'true')
            )
        !!}
            @include('gestion.modelos.create')
        
        {!! Form::close() !!}
      
@endsection   