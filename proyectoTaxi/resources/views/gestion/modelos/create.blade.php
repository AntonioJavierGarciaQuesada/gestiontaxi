    <div class="row">
        <div class="form-group col" >
            <label for="marca">Marca</label>
            <select name="marca" class="form-control custom-select" required>
                <option selected disabled>Elija una Marca...</option>
                <option value="Seat">Seat</option>
                <option value="Ford">Ford</option>
                <option value="Toyota">Toyota</option>
                <option value="Skoda">Skoda</option>
                <option value="Mercedes">Mercedes</option>
                <option value="Volkswagen">Volkswagen</option>
                <option value="Opel">Opel</option>
                <option value="Reanault">Renault</option>
                <option value="Citroen">Citroen</option>
                <option value="Audi">Audi</option>
                <option value="Volvo">Volvo</option>    
            </select>
        </div>
        <div class="form-group col">
            <label for="numPlazas">Número Plazas</label>
            <input type="number" value="3" class="form-control ml-2" min=3 max=6 name="num_plazas" required>
        </div> 
        <div class="form-group col">
            <label for="cilindrada" class="">Cilindrada</label>
            <select name="cilindrada" class="form-control custom-select" required>
                <option selected disabled>Cilindrada del motor....</option>
                <option value="1.4">1400</option>
                <option value="1.6">1600</option>
                <option value="1.8">1800</option>
                <option value="1.9">1900</option>
                <option value="2.0">2000</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="form-group col">
            <label for="modelo">Modelo</label>
            <input type="text" class="form-control" name="modelo" required>
        </div>        
        <div class="form-group col">
            <label for="descripcion">Descripción</label>
            <input type="text" class="form-control " name="descripcion" required>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label for="foto">Foto </label>
            <input name="foto" type="file" /> 
        </div>
    </div>
    
    <div class="row justify-content-center">
         <button class="btn btn-primary" >Crear Modelo</button>
   </div>