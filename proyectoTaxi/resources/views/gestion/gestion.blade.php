@extends('layouts.app')

@section('content')
    <section class="container-fluid">
		
			<div class="row justify-content-center">
				<div class="col-10 col-sm-6  mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/carreras"> Carreras</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/carrera.png"  class="tarjeta img-fluid col-10" alt="">
						</div>						
					</div>		
				</div>
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/estadosCarreras">Estado de una Carrera</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/estados.jpg"  class="tarjeta img-fluid col-10" alt="">
						</div>						
					</div>					
				</div>
				@if(Auth::user()!==null && Auth::user()->hasRole('admin'))
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/modelos">Modelos Coches</a>
					</div>
					<div class="row justify-content-center">						
						<div class="row justify-content-center">
							<img src="/img/foto/modelos2.png"  class="tarjeta img-fluid col-10 p3" alt="">
						</div>						
					</div>		
				</div>
				
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/taxis">Taxi</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/taxi3.png"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>		
				</div>
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/empleados">Empleado</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/empleados2.png"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>		
				</div>
				@endif
				<div class="col-10 col-sm-6  mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/turnos">Turnos</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/horario2.png"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>		
				</div>
				@if(Auth::user()!==null && Auth::user()->hasRole('admin'))
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center" >
						<a class="col-10 p-3" href="{{route('tarifas.index')}}">Tarifa</a>					
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/tarifa2.png"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>	
				</div>
				<div class="col-10  col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 pt-3 pb-3 " href="{{route('tipoEstado.index')}}"> Tipo Estado de Carrera</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/tipo.jpg"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>		
				</div>
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="gestion/recuperar/"> Recuperación Datos</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/db.jpg"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>		
				</div>
				<div class="col-10 col-sm-6 mt-4 mb-4">
					<div class="row justify-content-center">
						<a class="col-10 p-3" href="{{route('reparaciones.index')}}"> Partes Reparacion</a>
					</div>
					<div class="row justify-content-center" >						
						<div class="row justify-content-center">
							<img src="/img/foto/taller.jpg"  class="tarjeta img-fluid col-10 " alt="">
						</div>						
					</div>		
				</div>
				@endif
			</div> 	
	
	</section>

@endsection