<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Alquiler Taxi</title>
    <link rel="icon" href="/img/taxi.png">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <style>
    .consultas {
      height: 200px;
    }
        html, body{
                background: url('/img/fondoLogin.jpg') fixed;
                background-size:cover;
                padding: 0;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                font-size: 16px;
                height: 100vh;
                margin: 0;

        }
        .navbar{
                background-color:rgba(200,200,200,0.8);
                color:white;
                height:3.5rem;
                border-radius:0px;
        }
        #contenido-central{
            min-height: 40rem;
        }
        .tarjeta{
            min-height: 8vw;
			max-height: 12vw;
        }
		section div a{
			text-align: center;
			background-color: rgba(118, 119, 19,0.75);
			color:white;
			text-decoration: none;

		}

		section div a:hover{
			background-color: rgba(236, 238, 6,0.75);
			text-decoration: none;
			color:black;
		}

		section div :hover{
			border-color:2px rgb(236, 238, 6) solid;
		}

        li{
            display:inline-block;
        }
		.principal{
            background-color:rgba(50,50,50,0.1);
            max-height:200%;
            padding:0px;

        }
        .card {
            background-color:rgba(100,100,100,0.5);
            color: white;
            border-radius:10px;
             box-shadow: 0 4px 8px 4px rgba(0,0,0,0.2);
        }
        .card-title {
            border-radius:10px 10px 0 0;
        }
        .card hr{
            background-color: white;
        }
        .index-social{
    background-color:#2c2c2c;
    color:#fff;
}
.link-area p{

    font-size:12px;
}
.index-social a{
    color:#fff;
    font-size:12px;
    display:block;
    float:left;
    padding:10px;
}
.index-link h6{
    margin-top:1.5rem;
   color:#f1f1f1;
   text-align:left;

}
.index-link{
    background-color:#000;
}
.index-link ul{
    padding:0px;
}
.index-link ul li{
    list-style-type:none;
    display:block;
}
.index-link ul li a{
    background-color:black;
  text-decoration:none;
  font-size:10px;
  color:#fff;
  display:block;
  padding:5px 0;
  text-align:left;
}
.index-link ul li a:hover{
     text-decoration:underline;
}
.copy-c{
    padding-top:15px;
    margin-left:15px;
}
#app nav {
    background-color: rgb(218, 218, 218);
}
nav ul {
    font-weight:bold; 
}
#logo {
    color:rgb(102, 101, 61);
    font-weight: bold;
    font-size:1.2rem;
}


    </style>
</head>
<body >


  <div class="container-fluid principal">
    <div id="app">
        
         <nav class="navbar navbar-light navbar-toggleable-md  fixed-top bg-faded">
            <a id="logo" href="#app" class="col-2 nav-link">Alquier Taxis <i class="fas fa-taxi"></i></a>
            
            <div class="collapse navbar-collapse navbar-right justify-content-end" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">Register</a></li>

                        @else
                            <li><a  class="nav-link" href="/gestion"><i class="far fa-calendar-alt"></i> Gestion</a></li>
                            @if(Auth::user()!==null && Auth::user()->hasRole('admin'))
				            <li ><a class="nav-link" href="/consultas"><i class="fas fa-question-circle"></i> Consultas</a></li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                <i class="fas fa-user-circle"></i> {{ Auth::user()->name }}
                                </a>

                                <ul class="dropdown-menu">
                                    <li class="col-12">
                                        <a class="nav-link" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" class="form-inline">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                        
                    </ul>

                </div>
            </nav>

        </div>
       
     

        <div class="container-fluid mt-5" id="contenido-central">
            @yield('content')

        </div>


        <section class="index-link mr-0 ml-0 mt-5">
    <div class="container-fluid mr-0 ml-0 p-0 ">
        <div class="row mr-0 ml-0">
            <div class="col-md-3">
                <div class="link-area">
                    <h6>ABOUT US</h6>
                    <p>Building Consensus among your Senior leaders to leverage your digital strengths and work on gaps which are hindering your growth.</p>


                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h6>PRODUCT</h6>
                    <ul>
                    <li><a href="#"> Services-1</a></li>
                    <li><a href="#"> Services-1</a></li>
                    <li><a href="#"> Services-1</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h6>COMPANY</h6>
                    <ul>
                    <li><a href="#"> Home</a></li>
                    <li><a href="#"> Blog</a></li>
                    <li><a href="#"> About</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h6>LEARM MORE</h6>
                    <ul>
                    <li><a href="#"> Services-1</a></li>
                    <li><a href="#"> Services-1</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="index-social">
    <div class="container">
    <div class="row index-social-link text-center">
            <p class="copy-c">&copy; Antonio Javier García Quesada 2018.
Proyecto Alquiler Taxi.</p>
        </div>
        </div>
</section>


    </div>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
