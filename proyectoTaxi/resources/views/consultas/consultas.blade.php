@extends('layouts.app')

@section('content')

<section class="container mt-5">        
    <div class="row card-deck mt-2 mb-2">
        <div class="col-12 col-sm-6 mb-3">
            <div class="row justify-content-center">
                <div class="card mr-0 ml-0 pt-0 pr-0 pl-0 col-10" >
                    <h5 class="card-title bg-warning text-center text-white pt-2 pb-2">Consultas Sobre Carreras</h5>
                    <div class="card-block mb-0">           
                        <p class="card-text text-center">Consultas relacionadas con las carreras realizadas por taxista o un taxi.</p>
                        <p class="text-center h1 text-warning" ><i class="fas fa-taxi"></i></h1>
                        <hr>
                        <div class="row justify-content-center">
                            <a href="/consultas/consultasCarreras/" class="btn btn-warning">Consultar</a>   
                        </div>           
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-12 col-sm-6 mb-3">
            <div class="row justify-content-center">
                <div class="card mr-0 ml-0 pt-0 pr-0 pl-0 col-10 offset-1" >
                    <h5 class="card-title bg-warning text-center text-white pt-2 pb-2">Consultas Sobre Productividad</h5>
                    <div class="card-block mb-0">           
                        <p class="card-text text-center">Consultas relacionadas con horas trabajadas por empleados.</p>
                        <p class="text-center h1 text-warning" ><i class="fas fa-clock"></i></h1>
                        <hr>
                        <div class="row justify-content-center">
                            <a href="/consultas/consultasTaxi/" class="btn btn-warning">Consultar</a>   
                        </div>           
                    </div>
                </div>
            </div>    
        </div>
        <div class="col-12 col-sm-6 mb-3">
            <div class="row justify-content-center">
                <div class="card mr-0 ml-0 pt-0 pr-0 pl-0 col-10 offset-1" >
                    <h5 class="card-title bg-warning text-center text-white pt-2 pb-2">Consultas Sobre Ingresos</h5>
                    <div class="card-block mb-0">           
                        <p class="card-text text-center">Consultas relacionadas con los ingresos relacionados con las carreras. </p>
                        <p class="text-center h1 text-warning"><i class="fas fa-money-bill-alt"></i></h1>
                        <hr>
                        <div class="row justify-content-center">
                            <a href="/consultas/consultasIngresos/" class="btn btn-warning">Consultar</a>   
                        </div>           
                    </div>
                </div>
            </div>    
        </div>
        <div class="col-12 col-sm-6 mb-3">
            <div class="row justify-content-center">
                <div class="card mr-0 ml-0 pt-0  pr-0 pl-0 col-10 offset-1" >
                    <h5 class="card-title bg-warning text-center text-white pt-2 pb-2">Consultas Sobre Estados</h5>
                    <div class="card-block mb-0">           
                        <p class="card-text text-center">Consultas relacionadas con los estados de los taxis. Reparacion y Activos</p>
                        <p class="text-center h1 text-warning"><i class="fas fa-heartbeat"></i></h1>
                        <hr>
                        <div class="row justify-content-center">
                            <a href="/consultas/consultasEstado/" class="btn btn-warning">Consultar</a>   
                        </div>           
                    </div>
                </div>
            </div>    
        </div>
        <div class="col-12 col-sm-6 mb-3">
            <div class="row justify-content-center">
                <div class="card mr-0 ml-0 pt-0  pr-0 pl-0 col-10 offset-1" >
                    <h5 class="card-title bg-warning text-center text-white pt-2 pb-2">Consultas Sobre Turnos</h5>
                    <div class="card-block mb-0">           
                        <p class="card-text text-center">Consultas relacionadas con los turnos en determinadas fechas.</p>
                        <p class="text-center h1 text-warning"><i class="fas fa-calendar-alt"></i></h1>
                        <hr>
                        <div class="row justify-content-center">
                            <a href="/consultas/consultasTurnos/" class="btn btn-warning">Consultar</a>   
                        </div>           
                    </div>
                </div>
            </div>    
        </div> 
        <div class="col-12 col-sm-6 mb-3">
            <div class="row justify-content-center">
                <div class="card mr-0 ml-0 pt-0  pr-0 pl-0 col-10 offset-1" >
                    <h5 class="card-title bg-warning text-center text-white pt-2 pb-2">Consultas Sobre Empleados</h5>
                    <div class="card-block mb-0">           
                        <p class="card-text text-center">Consultas relacionadas con el asentismo de los empleados.</p>
                        <p class="text-center h1 text-warning"><i class="fas fa-users"></i></h1>
                        <hr>
                        <div class="row justify-content-center">
                            <a href="/consultas/consultasEmpleados/" class="btn btn-warning">Consultar</a>   
                        </div>           
                    </div>
                </div>
            </div>    
        </div>	
    </div>
</section>

    

@endsection