@extends('layouts.app')

@section('content')
    <div class="row">

    <div class="col-10 offset-1 mb-4 mt-4 pb-2">

        <a href="../" class="btn btn-success float-right ml-5" >Volver Consultas</a>

    </div>
    <div class="container">
        <div class="row mb-4">
            <div class="col-5 ml-5 form-control consultas p-3">
                <h5 class="col-12 text-center bg-info text-white pt-2 pb-2">Consultar Taxis Averiados</h5>
                {!! Form::open(array(
                            'url'     =>  'consultas/consultasEstado/averiados',
                            'method'    =>  'POST',
                            'class'     =>  'row mb-3 mt-3')
                        )
                    !!}
                    <div class="container">
                        <div class="row justify-content-center">
                                <input type="submit" class="btn btn-info" value="Consultar">

                        </div>
                    </div>
                    {!! Form::close() !!}

            </div>
            <div class="col-5 offset-1 form-control consultas p-3">
                <h5 class="col-12 text-center bg-info text-white pt-2 pb-2">Consultar Taxis Ocupados</h5>
                {!! Form::open(array(
                            'url'     =>  'consultas/consultasEstado/ocupados',
                            'method'    =>  'POST',
                            'class'     =>  'row mb-3 mt-3')
                        )
                    !!}
                    <div class="container">
                        <div class="row justify-content-center">
                                <input type="submit" class="btn btn-info" value="Consultar">

                        </div>
                    </div>

                    {!! Form::close() !!}

            </div>
    </div>
    @if (isset($taxisReparacion))
    <div class="row">

        <div class="col-10 offset-1">
            <h5 class="text-center">Taxis En Reparación</h5>
            <table class="table table-striped">
                    <tr >
                        <th>ID. Taxista</th>
                        <th>Matricula</th>
                        <th>Fecha Prevista</th>
                    </tr>
                    @foreach($taxisReparacion as $taxis)
                    <tr>
                        <td>{{$taxis->id_taxi}}</td>
                        <td>{{$taxis->matricula}}</td>
                        <td>{{$taxis->fecha_prevista}}</td>

                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
    @elseif(isset($taxisOcupados))
    <div class="row">

        <div class="col-10 offset-1">
        <h5 class="text-center" >Taxis Actualmente Ocupados
            <span><i class="fas fa-calendar-alt"></i> <?php echo date('d-m-Y');?></span>
            <span><i class="fas fa-clock"></i><?php echo date('h:i:s');?></span>
        </h5>
            <table class="table table-striped">
                    <tr >
                        <th>ID. Taxista</th>
                        <th>Matricula</th>
                        <th>Estado</th>
                    </tr>
                    @foreach($taxisOcupados as $taxis)
                    <tr>
                        <td>{{$taxis->id_taxi}}</td>
                        <td>{{$taxis->matricula}}</td>
                        <td>Ocupado</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
    @endif

@endsection
