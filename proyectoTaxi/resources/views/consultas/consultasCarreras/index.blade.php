@extends('layouts.app')

@section('content')
    <div class="row mt-5">

    <div class="col-10 offset-1  pb-2">

        <a href="../" class="btn btn-success float-right ml-5" >Volver Consultas</a>
    </div>
    <h2 class="col-10 offset-1">Consultas sobre Carreras</h2>
    <hr class="col-10 offset-1">
    </div>
    <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>      
    <div class="container mb-5 pb-5">
        <div class="form-control consultas">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Buscar carreras de un Taxi</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasCarreras/taxi',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-5 mt-3',
                        'validated')
                    )
                !!}
                   
                        <div class="form-inline col-5">
                            <label for="" class="mr-1 ml-5">Fecha inicio  <i class="fas fa-hourglass-start"></i></label>
                            <input type="date" class="form-control" value="<?php if(isset($datos[2]->fecha_inicio)){echo $datos[2]->fecha_inicio;};?>" name="fecha_inicio" required>
                        </div>
                
             
                        <div class="form-inline col">
                            <label for="" class="mr-1">Fecha fin  <i class="fas fa-hourglass-end"></i></label>
                            <input type="date"  class="form-control" value="<?php if(isset($datos[2]->fecha_fin)){echo $datos[2]->fecha_fin;};?>" name="fecha_fin" required>
                        </div>                 
     
                        <div class="input-group form-inline col">
                            <label for="" class="mr-2">Matrícula</label>
                            <select name="id_taxi" class="custom-select" id="matricula">
                                @foreach($datos[0] as $taxi)
                                    <option value="{{$taxi->id_taxi}}" <?php if(isset($datos[2]->id_taxi)&& $datos[2]->id_taxi==$taxi->id_taxi){echo 'selected';};?>>{{$taxi->matricula}}</option>
                                @endforeach
                            </select>
                      
                    </div>
                    <div class="container mt-4">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="consultar">
                        </div>
                    </div>

                    {!! Form::close() !!}
            </div>
            <div class="form-control consultas mt-5 mb-5">
            <h5 class="text-center text-white bg-info pt-2 pb-2">Buscar un Carrera</h5>
                {!! Form::open(array(
                            'url'     =>  'consultas/consultasCarreras/carrera',
                            'method'    =>  'POST',
                            'class'     =>  'row mb-3 mt-3')
                        )
                    !!}
                    <div class="col form-inline ml-5">
                        <label for="" class="mr-3">Identificador de Carrera</label>
                       <select name="id_carrera">
                                @foreach($datos[1] as $carrera)
                                    <option value="{{$carrera->id_carrera}}" <?php if(isset($datos[2]->id_carrera)&& $datos[2]->id_carrera==$carrera->id_carrera){echo 'selected';};?>>{{$carrera->id_carrera}}</option>
                                @endforeach
                       </select>


                    </div>
                    <div class="col mr-5">
                        <input type="submit" class="btn btn-info" value="Consultar">
                    </div>
                {!! Form::close() !!}

            </div>
         </div>


    <div class="col-10 offset-1 mt-3">
        <table class="table table-striped">
                <tr >
                    <th>Id Carrera</th>
                    <th>Tarifa</th>
                    <th>Turno</th>
                    <th>Controlador</th>
                    <th>origen</th>
                    <th>destino</th>

                </tr>

                    @if(isset($carrerasTaxi))
                        @foreach($carrerasTaxi as $carreraTaxi)
                            <tr>
                                <td>{{$carreraTaxi->cod_carrera}}</td>
                                <td>{{$carreraTaxi->cod_tarifa}}</td>
                                <td>{{$carreraTaxi->cod_turno}}</td>
                                <td>{{$carreraTaxi->cod_controlador}}</td>
                                <td>{{$carreraTaxi->origen}}</td>
                                <td>{{$carreraTaxi->destino}}</td>
                            </tr>

                        @endforeach
                    @elseif(isset($carreraUnica))

                          <tr>

                                <td>{{$carreraUnica->id_carrera}}</td>
                                <td>{{$carreraUnica->cod_tarifa}}</td>
                                <td>{{$carreraUnica->cod_turno}}</td>
                                <td>{{$carreraUnica->cod_controlador}}</td>
                                <td>{{$carreraUnica->origen}}</td>
                                <td>{{$carreraUnica->destino}}</td>
                            </tr>

                    @endif
        </table>
    </div>
    <div class="col-8 offset-2">

    </div>
    </div>
@endsection
<script>

    function manejadorEventoTaxi(e){
       var id_taxi=e.target.value;
       var link = document.getElementById('consultarTaxi');
       link.setAttribute('href','/consultas/consultasCarreras/taxi/'+id_taxi);
    }
    function manejadorEventoCarrera(e){
       var id_carrera=e.target.value;
       var link = document.getElementById('consultarCarrera');
       link.setAttribute('href','/consultas/consultasCarreras/carrera/'+id_carrera);

    }


    function init(){
      var  matriculaSl=document.getElementById('matricula');
      var carreraInput = document.getElementById('carr');
      matriculaSl.onblur = manejadorEventoTaxi;
      carreraInput.onblur = manejadorEventoCarrera;
    }
    window.onload= init;
</script>
