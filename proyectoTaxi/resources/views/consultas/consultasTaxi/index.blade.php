@extends('layouts.app')

@section('content')
    <div class="row">

    <div class="col-10 offset-1 mb-4 mt-4 pb-2">

        <a href="../" class="btn btn-success float-right ml-5 ">Volver Consultas</a>

    </div>
    <h2 class="col-10 offset-1">Consultas sobre Productividad</h2>
    <hr class="col-10 offset-1">
    <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>      
    <div class="container">
        <div class="row form-control consultas ml-2">
            <div class="col-12 ">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Listado General de Horas trabajadas</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasTaxi/consultaListadoGeneral',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-3 mt-3 pr-5 pl-5',
                        'validated')
                    )
                !!}                   
                        <div class="form-inline col">
                            <label for="" class="mr-3">Fecha inicio  <i class="fas fa-hourglass-start"></i></label>
                            <input type="date" class="form-control" name="fecha_inicio" 
                            value="<?php if(isset($datos[2]->fecha_inicio)){echo $datos[2]->fecha_inicio;};?>"
                            required>
                        </div>                
                
                        <div class="form-inline col">
                            <label for="" class="mr-3">Fecha fin  <i class="fas fa-hourglass-end"></i></label>
                            <input type="date"  class="form-control"  name="fecha_fin" 
                            value="<?php if(isset($datos[2]->fecha_inicio)){echo $datos[2]->fecha_inicio;};?>"
                            required>
                        </div>
                    <div class="container mt-4">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="Consultar">
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

        <div class="form-control consultas">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Horas Trabajadas por un Trabajador</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasTaxi/consultaListadoTrabajador',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-3 mt-3 pr-5 pl-5',
                        'validated')
                    )
                !!}
                   
                        <div class="form-group col">
                            <label for="" class="">Fecha inicio <i class="fas fa-hourglass-start"></i></label>                        
                            <input type="date" class="form-control" name="fecha_inicio"
                            value="<?php if(isset($datos[2]->fecha_inicio)){echo $datos[2]->fecha_inicio;};?>" required>
                        </div>
                
                        <div class="form-group col">
                            <label for="" class="mr-1">Fecha fin  <i class="fas fa-hourglass-end"></i></label>                     
                            <input type="date"  class="form-control" value="<?php if(isset($datos[2]->fecha_fin)){echo $datos[2]->fecha_fin;};?>" name="fecha_fin" required>
                        </div>
                    
                   
                        <div class="form-group col">
                            <label for="" class="mr-2">Nombre</label>
                            <select name="id_empleado" class="col custom-select">
                                @foreach($datos[0] as $taxista)
                                    <option value="{{$taxista->id_empleado}}" <?php if(isset($datos[2]->id_empleado)&& $datos[2]->id_empleado==$taxista->id_empleado){echo 'selected';};?>>{{$taxista->nombre}},{{$taxista->apellidos}}</option>
                                @endforeach
                            </select>
                        </div>
                    
                    <div class="container mt-1">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="Consultar">
                        </div>
                    </div>

                    {!! Form::close() !!}
            </div>
        </div>
        @if (isset($tiempoTotal))
        <div class="col-10 offset-1">
            <h5 class="pt-2 pb-2 text-center mt-2"> Número de Horas Totales Realizadas: {{$tiempoTotal}}</h5>
            <table class="table table-striped">
                    <tr >
                        <th>ID. Taxista</th>
                        <th>Nombre Completo</th>                        
                        <th>Num. Horas</th>

                    </tr>
                    @foreach($datos[1] as $turno)
                    <tr>
                        <td>{{$turno->id_empleado}}</td>
                        <td>{{$turno->nombre}}, {{$turno->apellidos}}</td>
                        <td>{{$turno->horasTotal}}</td>

                    </tr>
                    @endforeach
            </table>
        </div>
        @elseif (isset($tiempoTotalTrabajador))
        <div class="col-10 offset-1">
            <h5 class="pt-2 pb-2 text-center mt-2"> Número de Horas Totales Realizadas: {{$tiempoTotalTrabajador}}</h5>
            <table class="table table-striped">
                    <tr >
                        <th>ID. Taxista</th>
                        <th>Nombre Completo</th>
                        <th>Fecha</th>
                        <th>Num. Horas</th>

                    </tr>
                    @foreach($datos[1] as $turnoTrabajador)
                    <tr>
                        <td>{{$turnoTrabajador->id_empleado}}</td>
                        <td>{{$turnoTrabajador->nombre}}, {{$turnoTrabajador->apellidos}}</td>
                        <td>{{$turnoTrabajador->fecha_inicio}}</td>
                        <td>{{$turnoTrabajador->horasTotal}}</td>

                    </tr>
                    @endforeach
            </table>
        </div>
        @endif
    <div class="col-8 offset-2">

    </div>
    </div>
@endsection
