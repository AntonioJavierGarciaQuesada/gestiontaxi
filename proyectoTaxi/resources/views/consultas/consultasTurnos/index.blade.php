@extends('layouts.app')

@section('content')
    <div class="row mt-5">

    <div class="col-10 offset-1pb-2">

        <a href="../" class="btn btn-success float-right ml-5 ">Volver Consultas</a>

    </div>
    <h2 class="col-10 offset-1">Consultas Sobre Turnos</h2>
    <hr class="col-10 offset-1">
    <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger col-10 offset-1">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif        
        </div>     
    <div class="container">
        <div class="row form-control consultas ml-1">
            <div class="col-10 offset-1">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Listar Turnos Diario</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasTurnos/turnoDiario',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-3 mt-3',
                        'validated')
                    )
                !!}

                       <div class="form-inline col">
                            <label for="" class="mr-3">Fecha  <i class="fas fa-hourglass-start"></i></label>
                             <input type="date" name="fecha_inicio" value="<?php if(isset($request->fecha_inicio)){echo $request->fecha_inicio;};?>"  required>
                        </div>
                        <div class="col-4 col">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="consultar">
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

        <div class="form-control consultas">
             <div class="col-10 offset-1">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Listar Turnos Semanal</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasTurnos/turnoSemanal',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-3 mt-3',
                        'validated')
                    )
                !!}
                    
                        <div class="form-inline col">
                            <label for="" class="mr-1 ml-5">Fecha inicio  <i class="fas fa-hourglass-start"></i></label>
                            <input type="date"  name="fecha_inicio" value="<?php if(isset($request->fecha_inicio)){echo $request->fecha_inicio;};?>" required>
                        </div>
                    <div class="col">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="consultar">
                        </div>
                    </div>


                    {!! Form::close() !!}
            </div>
            </div>
        </div>

        <div class="col-10 offset-1">
            <h5 class="pt-2 pb-2 text-center mt-2"> Turnos Realizadas</h5>
            <table class="table table-striped">
                    <tr >
                        <th>ID. Turno</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <th>ID. Taxista</th>
                        <th>ID. Taxi</th>
                    </tr>
                    @if (isset($turnosDiario))
                        @foreach($turnosDiario as $turnoDiario)
                        <tr>
                            <td>{{$turnoDiario->id_turno}}</td>
                            <td>{{$turnoDiario->fecha_inicio}} {{$turnoDiario->hora_inicio}}</td>
                            <td>{{$turnoDiario->fecha_fin}} {{$turnoDiario->hora_fin}}</td>
                            <td>{{$turnoDiario->cod_taxista}}</td>
                            <td>{{$turnoDiario->cod_taxi}}</td>

                        </tr>
                        @endforeach
                     @elseif (isset($turnosSemanal))
                        @foreach($turnosSemanal as $turnoSemanal)
                            <tr>
                                <td>{{$turnoSemanal->id_turno}}</td>
                                <td>{{$turnoSemanal->fecha_inicio}} {{$turnoSemanal->hora_inicio}}</td>
                                <td>{{$turnoSemanal->fecha_fin}} {{$turnoSemanal->hora_fin}}</td>
                                <td>{{$turnoSemanal->cod_taxista}}</td>
                                <td>{{$turnoSemanal->cod_taxi}}</td>

                            </tr>
                        @endforeach
                    @endif
            </table>
        </div>

    </div>
@endsection
