@extends('layouts.app')

@section('content')
    <div class="row">

    <div class="col-10 offset-1 mb-4 mt-4 pb-2">

        <a href="../" class="btn btn-success float-right ml-5" >Volver Consultas</a>

    </div>
    <h2 class="col-10 offset-1">Consultas sobre Empeados</h2>
    <hr class="col-10 offset-1">
    <div class="container">
        <div class="row mb-4">
            <div class="col-3 ml-5 form-control consultas p-3">
                <h5 class="col-12 text-center bg-info text-white pt-2 pb-2">Consultar Empleados de Baja</h5>
                {!! Form::open(array(
                            'url'     =>  'consultas/consultasEmpleados/asentismo',
                            'method'    =>  'POST',
                            'class'     =>  'row mb-3 mt-3')
                        )
                    !!}
                    <div class="container">
                        <div class="row justify-content-center">
                                <input type="submit" class="btn btn-info" value="Consultar">

                        </div>
                    </div>
                    {!! Form::close() !!}

            </div>
            <div class="col-3 offset-1 form-control consultas p-3">
                <h5 class="col-12 text-center bg-info text-white pt-2 pb-2">Consultar Facturacion Por Empleado</h5>
                {!! Form::open(array(
                            'url'     =>  'consultas/consultasEmpleados/facturacionMensual',
                            'method'    =>  'POST',
                            'class'     =>  'row mb-3 mt-3')
                        )
                    !!}
                    <div class="container">
                        <div class="row justify-content-center">
                                <input type="submit" class="btn btn-info" value="Consultar">

                        </div>
                    </div>

                    {!! Form::close() !!}

            </div>
            <div class="col-3 offset-1 form-control  consultas p-3">
                <h5 class="col-12 text-center bg-info text-white pt-2 pb-2">Consultar Empleados de Vacaciones</h5>
                {!! Form::open(array(
                            'url'     =>  'consultas/consultasEmpleados/vacaciones',
                            'method'    =>  'POST',
                            'class'     =>  'row mb-3 mt-3')
                        )
                    !!}
                    <div class="container">
                        <div class="row justify-content-center">
                                <input type="submit" class="btn btn-info" value="Consultar">

                        </div>
                    </div>

                    {!! Form::close() !!}

            </div>
    </div>
    @if (isset($empleadosBaja))
    <div class="row">

        <div class="col-10 offset-1">
            <h5 class="text-center">Empleados De Baja
                <span><i class="fas fa-calendar-alt"></i> <?php echo date('d-m-Y');?></span>
                <span><i class="fas fa-clock"></i><?php echo date('h:i:s');?></span>
            </h5>
            <table class="table table-striped">
                    <tr >
                        <th>ID. Empleado</th>
                        <th>Nº Seg. Social</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                    </tr>
                    @foreach($empleadosBaja as $empleadoBaja)
                    <tr>
                        <td>{{$empleadoBaja->id_empleado}}</td>
                        <td>{{$empleadoBaja->num_seg_social}}</td>
                        <td>{{$empleadoBaja->nombre}},{{$empleadoBaja->apellidos}}</td>
                        <td>{{$empleadoBaja->telefono}}</td>

                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
    @elseif(isset($empleadosVacaciones))
    <div class="row">

        <div class="col-10 offset-1">
        <h5 class="text-center" >Empleados Vacaciones
            <span><i class="fas fa-calendar-alt"></i> <?php echo date('d-m-Y');?></span>
            <span><i class="fas fa-clock"></i><?php echo date('h:i:s');?></span>
        </h5>
            <table class="table table-striped">
            <tr >
                        <th>ID. Empleado</th>
                        <th>Nº Seg. Social</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                    </tr>
                    @foreach($empleadosVacaciones as $empleadoVacaciones)
                    <tr>
                        <td>{{$empleadoVacaciones->id_empleado}}</td>
                        <td>{{$empleadoVacaciones->num_seg_social}}</td>
                        <td>{{$empleadoVacaciones->nombre}},{{$empleadoVacaciones->apellidos}}</td>
                        <td>{{$empleadoVacaciones->telefono}}</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
    @elseif (isset($empleadosFacturacion))
    <div class="row">

           <div class="col-10 offset-1">
           <h5 class="text-center" >Listados Empleado Y sus Facturaciones
               <span><i class="fas fa-calendar-alt"></i> <?php echo date('d-m-Y');?></span>
               <span><i class="fas fa-clock"></i><?php echo date('h:i:s');?></span>
           </h5>
               <table class="table table-striped">
               <tr >
                           <th>ID. Empleado</th>
                           <th>Nº Seg. Social</th>
                           <th>Nombre</th>
                           <th>Teléfono</th>
                           <th>Total Facturado</th>
                       </tr>
                       @foreach($empleadosFacturacion as $empleadoFacturacion)
                       <tr>
                           <td>{{$empleadoFacturacion->id_empleado}}</td>
                           <td>{{$empleadoFacturacion->num_seg_social}}</td>
                           <td>{{$empleadoFacturacion->nombre}},{{$empleadoFacturacion->apellidos}}</td>
                           <td>{{$empleadoFacturacion->telefono}}</td>
                           <td>{{$empleadoFacturacion->TotalFacturado}}</td>
                       </tr>
                       @endforeach
               </table>
           </div>
       </div>
      
      
    @endif
    </div>
@endsection
