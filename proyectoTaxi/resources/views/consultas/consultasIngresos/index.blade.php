@extends('layouts.app')

@section('content')
    <div class="row mt-5">

    <div class="col-10 offset-1 mb-4 pb-2">

        <a href="../" class="btn btn-success float-right ml-5 " >Volver Consultas</a>

    </div>
    <h2 class="col-10 offset-1">Consultas Sobre Ingresos</h2>
    <hr class="col-10 offset-1">
    <div class="container">
        <div class="row ml-2">
            <div class="col-8">

            <div class="col-12 form-control consultas">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Ingreso Medio por Carrera</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasIngresos/IngresoMedio',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-3 mt-3',
                        'validated')
                    )
                !!}

                    <div class="col">
                        <div class="input-group form-inline ml-3">
                            <label for="" class="mr-3">Fecha inicio</label>
                            <span class="input-group-addon">
                            <i class="fas fa-hourglass-start"></i>
                            </span>
                            <input type="date" aria-label="Text input with checkbox" name="fecha_inicio" required>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group form-inline ml-3">
                        <label for="" class="mr-3">Fecha fin</label>
                        <span class="input-group-addon">
                            <i class="fas fa-hourglass-end"></i>
                        </span>
                        <input type="date"  aria-label="Text input with radio button"  name="fecha_fin" required>
                        </div>
                    </div>
                    <div class="container mt-3">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="consular">
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>

                <div class="col-12 form-control consultas">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Ingreso Total en Periodo</h5>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasIngresos/IngresoTotal',
                        'method'    =>  'POST',
                        'class'     =>  'row mb-3 mt-3',
                        'validated')
                    )
                !!}

                    <div class="col form-inline ml-3">
                            <label for="" class="mr-3">Fecha inicio <i class="fas fa-hourglass-start"></i></label>
                            <input type="date" aria-label="Text input with checkbox" name="fecha_inicio"
                            value="<?php if(isset($datos[2]->fecha_inicio)){echo $datos[2]->fecha_inicio;};?>"
                             required>
                        </div>
                    </div>
                    
                        <div class="col form-inline ml-3">
                             <label for="" class="mr-1">Fecha fin <i class="fas fa-hourglass-end"></i></label>                
                             <input type="date"  class="form-control" value="<?php if(isset($datos[2]->fecha_fin)){echo $datos[2]->fecha_fin;};?>" name="fecha_fin" required>
                        </div>
                    
                    <div class="container mt-3">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md" value="consular">
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
                </div>
                <div class="col-4 form-control consultas">
                <h5 class="text-center bg-info text-white pt-2 pb-2">Productividad Zonas</h5>
                <p class="text-justify mt-2  ml-3 mr-3 "> Listado de todas las zonas en orden, dependiendo de donde se producen más carreras.</p>
                <hr>
                {!! Form::open(array(
                        'url'     =>  'consultas/consultasIngresos/Productividad',
                        'method'    =>  'POST',
                        'class'     =>  'row ',
                        'validated')
                    )
                !!}
                <div class="container ">
                        <div class="row justify-content-center">
                            <input type="submit" class="btn btn-info btn-md " value="consular">
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="row mt-4">
                @if (isset($ingresoTotal))

                    <h4 class="col text-center">Ingreso Total es de  {{$ingresoTotal}}€</h4>

                @elseif (isset($ingresoMedio))
                    <h4 class="col text-center">Ingreso Medio por carrera es de  {{$ingresoMedio}}€</h4>
                @elseif (isset($zonasListado))
                    <div class="container">
                        <div class="row">
                             <div class="col-6 offset-3">
                             <table class="table table-striped">
                                <tr >
                                    <th class="text-center">Zona</th>
                                    <th class="text-center"> Número Carreras</th>
                                </tr>
                                @foreach($zonasListado as $zona)
                                <tr>
                                    <td class="text-center">{{$zona->zona}}</td>
                                    <td class="text-center">{{$zona->numCarreras}} </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>

                @endif
             </div>
        </div>


@endsection
