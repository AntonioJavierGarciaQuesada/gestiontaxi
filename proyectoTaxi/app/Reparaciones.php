<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reparaciones extends Model
{
    protected $table = 'reparaciones';
    protected $primaryKey = 'id_reparacion';
    protected $fillable = [
        'fecha_entrada',
        'fecha_salida',
        'fecha_prevista',
        'coste_reparacion',
        'cod_taxi'
    ];

    public $timestamps = false;
}
