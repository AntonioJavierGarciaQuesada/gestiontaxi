<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarifas extends Model
{
    protected $table = 'tarifas';
    protected $primaryKey = 'id_tarifa';
    protected $fillable = [
        'tipo','descripcion','precio_minuto'
    ];
    public $timestamps = false;
}
