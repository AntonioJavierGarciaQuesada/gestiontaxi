<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxis extends Model
{
    protected $table = 'taxis';
    protected $primaryKey = 'id_taxi';
    protected $fillable = [
        'num_licencia_taxi',
        'matricula',
        'estado',
        'year',
        'propietario',
        'datafono',
        'cod_modelo',
        'disponibilidad'
        
    ];
    
    public $timestamps = false;
}
