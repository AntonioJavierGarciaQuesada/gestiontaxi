<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    protected $table = 'empleados';
    protected $primaryKey = 'id_empleado';
    protected $fillable = [
        'num_seg_social',
        'puesto',
        'num_Licencia',        
        'nombre',
        'apellidos',
        'telefono',
        'fecha_nacimiento',
        'fecha_contratacion',
        'email',
        'direccion',
        'situacion',
        'foto',
        'borrado'
        ,
        
    ];
    public $timestamps = false;

    public function taxistasTurnos()
{
    return $this->belongsTo('App\Turnos', 'cod_taxista', 'id_empleado');
}
}
