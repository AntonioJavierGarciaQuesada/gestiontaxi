<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carreras extends Model
{
    protected $table = 'carreras';
    protected $primaryKey = 'id_carrera';
    protected $fillable = [
        'duracion',
        'precio',
        'tipo_cliente',
        'origen',
        'destino',
        'gps_origen',
        'gps_destino',
        'tipo_pago',        
        'cod_tarifa',
        'cod_turno',
        'cod_controlador',
        'zona'
    ];
    public $timestamps = false;
}
