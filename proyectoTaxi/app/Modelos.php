<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelos extends Model
{
    protected $table = 'modelos';
    protected $primaryKey = 'id_coche';
    protected $fillable = [
        'marca','modelo','num_plazas','cilindrada','descripcion','foto'
    ];
    public $timestamps = false;
}
