<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEstado extends Model
{
    protected $table = 'tipo_estado';
    protected $primaryKey = 'id_tipo_estado';
    protected $fillable = [
        'tipo','descripcion'
    ];
    public $timestamps = false;
}
