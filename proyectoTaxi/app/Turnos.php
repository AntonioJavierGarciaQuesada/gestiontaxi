<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turnos extends Model
{
    protected $table = 'turnos';
    protected $primaryKey = 'id_turno';
    protected $fillable = [
        'fecha_inicio',
        'hora_inicio',
        'fecha_fin',
        'hora_fin',
        'hora_inicio_real',
        'hora_fin_real',
        'cod_taxista',
        'cod_taxi'
    ];
    public $timestamps = false;
}
