<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Taxis;
use App\TipoEstado;
use App\Carreras;
use App\EstadosCarreras;
use Illuminate\Support\Facades\DB;

class estadosCarrerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estadosCarreras= ESTADOSCARRERAS::orderBy('id_estado_carrera','DESC')
        ->where('borrado','=',0)
        ->paginate(5);
        
        return view('gestion.estadosCarreras.index',compact('estadosCarreras'))
        ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $carreras = DB::select('SELECT * from carreras 
        where id_carrera not in (SELECT cod_carrera from estados_carrera WHERE borrado=0 and cod_estado =2);
        ');
        $estados = TIPOESTADO::where('borrado','=',0)->get();
        
        $datos=[$estados,$carreras];
        return view('gestion.estadosCarreras.Layoutcreate',compact('datos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ESTADOSCARRERAS::create($request->all());
       
        if($request->cod_estado == "2"){
            $cod_taxi = DB::select('SELECT  DISTINCT cod_taxi 
            from taxis join turnos on taxis.id_taxi= turnos.cod_taxi 
            join carreras on turnos.id_turno = carreras.cod_turno 
            join estados_carrera on carreras.id_carrera = estados_carrera.cod_carrera 
            where cod_carrera ="'.$request->cod_carrera.'"');            
            $taxi=DB::select('UPDATE taxis set disponibilidad=false where id_taxi='.$cod_taxi[0]->cod_taxi);
        }
        return redirect()->route('estadosCarreras.index')
        ->with('sucess','Estado de la Carrera creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_estado_carrera)
    {        
        
        $estadoCarrera = ESTADOSCARRERAS::find($id_estado_carrera);          
        
        $carrera = CARRERAS::find($estadoCarrera->cod_carrera);
        $estado = TIPOESTADO::find($estadoCarrera->cod_estado);
        
        $datos=[$estado,$carrera];
        
        return view('gestion.estadosCarreras.show',compact('estadoCarrera'),compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_estado_carrera)
    {
        $estadoCarrera = ESTADOSCARRERAS::find($id_estado_carrera);    
        $carreras =  $turnos = DB::select('SELECT * FROM carreras,(SELECT DISTINCT cod_carrera from estados_carrera WHERE cod_estado = 2 ) carreras_iniciadas 
        where id_carrera != carreras_iniciadas.cod_carrera');
        $estados = TIPOESTADO::where('borrado','=',0)->get();
        
        $datos=[$estados,$carreras];

        return view('gestion.estadosCarreras.Layoutactualizar',compact('estadoCarrera'),compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_estado_carrera)
    {
        $estadoCarrera= ESTADOSCARRERAS::find($id_estado_carrera);        
        $estadoCarrera->update($request->all());
        return redirect()->route('carreras.index')
        ->with('success','Estado de la Carrera actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_estado_carrera)
    {
        $estadoCarrera = ESTADOSCARRERAS::find($id_estado_carrera);
        $estadoCarrera->borrado=true;
        $estadoCarrera->save();
        
        return redirect()->route('estadosCarreras.index')
        ->with('success','Estado de la Carrera borrada correctamente');
    }
}
