<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Modelos;
use App\Taxis;

class taxisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxisListado = TAXIS::join('modelos','cod_modelo','=','id_coche')
        ->where('taxis.borrado',"=",0)->getQuery()->paginate(5);

        return view('gestion.taxis.index',compact('taxisListado'))
        ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modelos= MODELOS::all();
        return view('gestion.taxis.Layoutcreate',compact('modelos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'num_licencia_taxi' => 'unique:taxis',
            'matricula' => 'unique:taxis'
        ]);
        

        if ($validator->fails()) {
            return redirect('/gestion/taxis/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        TAXIS::create($request->all());
        return redirect()->route('taxis.index')
        ->with('sucess','Taxi creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_taxi)
    {

        $taxis = TAXIS::join('modelos','cod_modelo','=','id_coche')
        ->where('id_taxi',"=",$id_taxi)->getQuery()->get();
        $taxi=$taxis[0];

        return view('gestion.taxis.show',compact('taxi'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_taxi)
    {
        
        $taxis = TAXIS::join('modelos','cod_modelo','=','id_coche')
        ->where('id_taxi',"=",$id_taxi)->getQuery()->get();
        $taxi=$taxis[0];
        $modelos = MODELOS::all();

        return view('gestion.taxis.Layoutactualizar',compact('taxi'),compact('modelos'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_taxi)
    {
        
       $taxi= TAXIS::find($id_taxi);

       $taxi->update($request->all());
       return redirect()->route('taxis.index')
       ->with('success','Taxi actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_taxi)
    {
        $taxi = TAXIS::find($id_taxi);
        $taxi->borrado=true;
        $taxi->save();
        
        return redirect()->route('taxis.index')
        ->with('success','Taxi borrado correctamente');
    }
}
