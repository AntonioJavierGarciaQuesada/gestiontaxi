<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Carreras;

class consultasIngresosController extends Controller
{
    public function consultarIngresoTotal(Request $request){
        $datos = DB::select("SELECT SUM(precio) ingresoTotal FROM `carreras` join estados_carrera on carreras.id_carrera=estados_carrera.cod_carrera WHERE fecha BETWEEN  '".$request->fecha_inicio."' AND '".$request->fecha_fin."'");
        $ingresoTotal = $datos[0]->ingresoTotal;
       return view('consultas.consultasIngresos.index',compact('ingresoTotal'));
    

    }

    public function consultarIngresoMedio(Request $request){
       $datos = DB::select("SELECT AVG(precio) ingresoMedio FROM `carreras` join estados_carrera on carreras.id_carrera=estados_carrera.cod_carrera WHERE fecha BETWEEN  '".$request->fecha_inicio."' AND '".$request->fecha_fin."'");        
       $ingresoMedio = $datos[0]->ingresoMedio;
       return view('consultas.consultasIngresos.index',compact('ingresoMedio'));        
    }
    public function consultarProductividad(Request $request){
        $zonasListado = DB::select("SELECT carreras.zona,count(carreras.zona) numCarreras 
        FROM carreras GROUP By zona ORDER BY 2 ASC;");        
        
        return view('consultas.consultasIngresos.index',compact('zonasListado'));        
     } 
    
    public function index(){
        
        return view('consultas.consultasIngresos.index');
          
    }   
    
}
