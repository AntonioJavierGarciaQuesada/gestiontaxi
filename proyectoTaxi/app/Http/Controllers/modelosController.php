<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos;
use File;
use Validator;

class modelosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelos = MODELOS::where("borrado","=",false)->paginate(5);
        return view('gestion.modelos.index',compact('modelos'))
            ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestion.modelos.Layoutcreate');           
    }


   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'cilindrada' => 'required',
            'marca' => 'required'
                       
        ])->validate(); 
        MODELOS::create($request->all());
        $modelo = MODELOS::all()->last();
        if($request->hasFile('foto')){
            $path="modelos";
            $file = $request->foto;
            $extension = $file->getClientOriginalExtension();
            $fileName = "foto".$modelo->id_coche.".".$extension;
            $file->move($path,$fileName);
            $modelo->foto="/".$path."/".$fileName;
            $modelo->save();
        }
        
        return redirect()->route('modelos.index')
            ->with('success','Modelo creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_coche)
    {
        $modelo = MODELOS::find($id_coche);
        return view('gestion.modelos.show',compact('modelo'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_coche)
    {
        $modelo = MODELOS::find($id_coche);
        return view('gestion.modelos.Layoutactualizar',compact('modelo'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_coche)
    {
        $modelo=MODELOS::find($id_coche);
        
        $modelo->update($request->all());
        return redirect()->route('modelos.index')
            ->with('success','Modelo actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_coche)
    {
        $modelo=MODELOS::find($id_coche);
        $modelo->borrado=true;
        $modelo->save();        
        
        //MODELOS::find($id_coche)->delete();
        return redirect()->route('modelos.index')
            ->with('success','Modelo borrado correctamente');
    }
}
