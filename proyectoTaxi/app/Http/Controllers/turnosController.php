<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turnos;
use App\Taxis;
use App\Empleados;
use Validator;

class turnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $turnosListado = TURNOS::join('empleados','cod_taxista','=','id_empleado')
        ->join('taxis',"cod_taxi","=","id_taxi")
        ->where('turnos.borrado',"=",0)->getQuery()->paginate(5);         

        return view('gestion.turnos.index',compact('turnosListado'))
        ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $taxis = TAXIS::join('modelos','id_coche','=','cod_modelo')
        ->where('taxis.borrado',"=",0)
        ->where('estado',"=",0)->get();
        $taxistas = EMPLEADOS::where('empleados.borrado','=',0)
        ->where('empleados.situacion','=','activo')
        ->whereNotNull('empleados.num_Licencia')->get();
        return view('gestion.turnos.Layoutcreate',compact('taxis'),compact('taxistas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'fecha_inicio' => 'beforeOrEqual:fecha_fin',
            'fecha_fin' => 'afterOrEqual:fecha_inicio',
           
        ]);
       

        if ($validator->fails()) {
            return redirect('/gestion/turnos/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        TURNOS::create($request->all());
        return redirect()->route('turnos.index')
        ->with('sucess','Turno creado correctamente');
    }




    private function inicio_fin_semana($fecha){
        
            $diaInicio="Monday";
            $diaFin="Sunday";
        
            $strFecha = strtotime($fecha);
        
            $fechaInicio = date('Y-m-d',strtotime('last '.$diaInicio,$strFecha));
            $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));
        
            if(date("l",$strFecha)==$diaInicio){
                $fechaInicio= date("Y-m-d",$strFecha);
            }
            if(date("l",$strFecha)==$diaFin){
                $fechaFin= date("Y-m-d",$strFecha);
            }
            return Array("fechaInicio"=>$fechaInicio,"fechaFin"=>$fechaFin);
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_taxi)
    {
        $hoy = date("Y-m-d"); 
        $fechas_inicio_fin_semana = $this->inicio_fin_semana($hoy);

        $turnosListado = TURNOS::join('empleados','cod_taxista','=','id_empleado')
        ->join('taxis',"cod_taxi","=","id_taxi")
        ->where('turnos.borrado','=',0)
        ->where('turnos.fecha_inicio','>=',$fechas_inicio_fin_semana['fechaInicio'])
        ->where('turnos.fecha_fin','<=',$fechas_inicio_fin_semana['fechaFin'])
        ->getQuery()->paginate(5);     
        

        return view('gestion.turnos.show',compact('turnosListado'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_turno)
    {
        $turno = TURNOS::find($id_turno); 
                       
        $taxis = TAXIS::join('modelos','cod_modelo','=','id_coche')
        ->where('taxis.borrado',"=",0)->get();
        
        $empleados = EMPLEADOS::where('borrado','=',0)
        ->where('situacion','=','activo')
        ->whereNotNull('num_Licencia')
        ->get();
        $datos=[$empleados,$taxis];
        return view('gestion.turnos.Layoutactualizar',compact('turno'),compact('datos'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_turno)
    {
       $turno= TURNOS::find($id_turno);
       $validator = Validator::make($request->all(),
       [
           'fecha_inicio' => 'beforeOrEqual:fecha_fin',
           'fecha_fin' => 'afterOrEqual:fecha_inicio'
       ])->validate();
       
       $turno->update($request->all());
       return redirect()->route('turnos.index')
       ->with('success','Turno actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_turno)
    {
        
        $taxi = TURNOS::find($id_turno);
        $taxi->borrado=true;
        $taxi->save();
        
        return redirect()->route('turnos.index')
        ->with('success','Turno borrado correctamente');
    }
}
