<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Turnos;
use App\Empleados;
use Validator;


class consultasTaxiController extends Controller
{
    private function calcularNumHoraTotales($request){
        $numHorasTotal = DB::select("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(tiempos.tiempo))) tiempo_total FROM
        (SELECT id_turno,timediff(concat(turnos.fecha_fin,' ',turnos.hora_fin),concat(turnos.fecha_inicio,' ',turnos.hora_inicio)) tiempo
        FROM `turnos` where borrado = 0 and turnos.fecha_inicio BETWEEN  '".$request->fecha_inicio."' AND '".$request->fecha_fin."') tiempos");
        return $numHorasTotal[0]->tiempo_total;
    }

    public function listadoNumHoras($request){
        $datos = DB::select("SELECT id_empleado,nombre,apellidos,id_turno,fecha_inicio,fecha_fin,timediff(concat(turnos.fecha_fin,' ',turnos.hora_fin),concat(turnos.fecha_inicio,' ',turnos.hora_inicio))  horasTotal FROM 
        `turnos` join empleados on turnos.cod_taxista= empleados.id_empleado where turnos.borrado = 0 and turnos.fecha_inicio BETWEEN '".$request->fecha_inicio."' AND '".$request->fecha_fin."'");
              
      
        return $datos;
    }
    
    private function calcularNumHoraTotalesTrabajador($request){
        $numHorasTotal = DB::select("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(tiempos.tiempo))) tiempo_total FROM
        (SELECT id_turno,timediff(concat(turnos.fecha_fin,' ',turnos.hora_fin),concat(turnos.fecha_inicio,' ',turnos.hora_inicio)) tiempo
        FROM `turnos` where borrado = 0 and turnos.fecha_inicio BETWEEN  '".$request->fecha_inicio."' AND '".$request->fecha_fin."' AND cod_taxista='".$request->id_empleado."') tiempos");
        return $numHorasTotal[0]->tiempo_total;

    }
    public function listadoNumHorasTrabajador($request){
            
        $datos = DB::select("SELECT id_empleado,nombre,apellidos,id_turno,fecha_inicio,fecha_fin,timediff(concat(turnos.fecha_fin,' ',turnos.hora_fin),concat(turnos.fecha_inicio,' ',turnos.hora_inicio))  horasTotal FROM 
        `turnos` join empleados on turnos.cod_taxista= empleados.id_empleado where turnos.borrado = 0 and turnos.fecha_inicio BETWEEN '".$request->fecha_inicio."' AND '".$request->fecha_fin."' AND id_empleado='".$request->id_empleado."'");       
       
         return $datos;
     }
    
    public function index(){
        $taxistas=EMPLEADOS::where('borrado','=','0')->get();
        $datos=[$taxistas];
        return view('consultas.consultasTaxi.index',compact('datos'));
          
    }   
    public function consultarGeneral(Request $request){
        $validator = Validator::make($request->all(),
    [
        'fecha_inicio' => 'beforeOrEqual:fecha_fin',
        'fecha_fin' => 'afterOrEqual:fecha_inicio',
       
    ])->validate();
        $tiempoTotal= $this->calcularNumHoraTotales($request);
        $turnos= $this->listadoNumHoras($request);
        $taxistas=EMPLEADOS::where('borrado','=','0')->get();
        $datos = [$taxistas,$turnos,$request];
      
        return view('consultas.consultasTaxi.index',compact('tiempoTotal'),compact('datos'));
    }
    public function consultarTrabajador(Request $request){
        $validator = Validator::make($request->all(),
    [
        'fecha_inicio' => 'beforeOrEqual:fecha_fin',
        'fecha_fin' => 'afterOrEqual:fecha_inicio',
       
    ])->validate();
        $tiempoTotalTrabajador= $this->calcularNumHoraTotalesTrabajador($request);
        $turnosTrabajador = $this->listadoNumHorasTrabajador($request);
        $taxistas=EMPLEADOS::where('borrado','=','0')->get();
        $datos = [$taxistas,$turnosTrabajador,$request];
        return view('consultas.consultasTaxi.index',compact('tiempoTotalTrabajador'),compact('datos'));
    }
}
