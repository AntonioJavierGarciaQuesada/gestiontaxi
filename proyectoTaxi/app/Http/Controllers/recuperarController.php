<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos;
use App\Taxis;
use App\Empleados;
use App\Tarifas;
use App\Carreras;
use App\EstadosCarrera;
use App\Reparaciones;
use App\Turnos;
use App\TipoEstado;
use Illuminate\Support\Facades\DB;

class recuperarController extends Controller
{
    function index(){
        $datos=null;
        return view('gestion.recuperar.index',compact('datos'));
    }
    public function edit(Request $request)
    {
        $tabla = DB::select("Select * From ".$request->tabla." Where borrado=1");        
        $datos= $request;
        return view('gestion.recuperar.index',compact('tabla'),compact('datos'));
    }

    public function update($tabla,$id_carrera){
        $carrera=CARRERAS::find($id_carrera);
        $carrera->borrado=0;
        $carrera->save();
        $datos = carrera;
        $tabla = DB::select("Select * From ".$request->tabla." Where borrado=1");        
        return view('gestion.recuperar.index',compact('datos'),compact('tabla'));
    }
}
