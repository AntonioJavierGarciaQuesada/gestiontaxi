<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empleados;
use Illuminate\Support\Facades\DB;

class consultasEmpleadosController extends Controller
{
    public function consultarAsentismo(Request $request){
        $empleadosBaja = EMPLEADOS::where('borrado','=','0')
        ->where('situacion','=','baja')->get();
        return view('consultas.consultasEmpleados.index',compact('empleadosBaja'));
    }

    
      
    public function consultarVacaciones(Request $request){
        $empleadosVacaciones = EMPLEADOS::where('borrado','=','0')
        ->where('situacion','=','vacaciones')->get();
        return view('consultas.consultasEmpleados.index',compact('empleadosVacaciones'));
    }
    public function consultarFacturacionMensual(Request $request){
        $empleadosFacturacion = DB::select("SELECT id_empleado,nombre,apellidos,num_seg_social,telefono,(Facturacion.total) TotalFacturado
        FROM empleados,
        (SELECT SUM(carreras.precio) total,turnos.cod_taxista From empleados 
        join turnos on empleados.id_empleado=turnos.cod_taxista 
        join carreras on turnos.id_turno = carreras.cod_turno
        join estados_carrera on estados_carrera.cod_carrera= carreras.id_carrera
        WHERE empleados.borrado=0 and turnos.borrado=0 and carreras.borrado=0 and estados_carrera.cod_estado=2
        GROUP By turnos.cod_taxista) Facturacion
        where empleados.id_empleado=Facturacion.cod_taxista
        ORDER By 6");
        return view('consultas.consultasEmpleados.index',compact('empleadosFacturacion'));
    }
    public function index(){
        return view('consultas.consultasEmpleados.index');
    }
}
