<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reparaciones;
use App\Taxis;
use Validator;

class reparacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reparacionesListado = REPARACIONES::join('taxis','cod_taxi','=','id_taxi')
        ->where('reparaciones.borrado','=',0)->getQuery()->paginate(5);

        return view('gestion.reparaciones.index',compact('reparacionesListado'))
        ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $taxis = TAXIS::where('taxis.borrado',"=",0)
        ->where('estado',"=",0)->get();
        return view('gestion.reparaciones.Layoutcreate',compact('taxis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'fecha_entrada' => 'beforeOrEqual:fecha_salida',
            'fecha_entrada' => 'beforeOrEqual:fecha_prevista',
           
        ])->validate();
       
        REPARACIONES::create($request->all());
        $taxi=TAXIS::find($request->cod_taxi);
        $taxi->estado=1;
        $taxi->save();

        return redirect()->route('reparaciones.index')
        ->with('sucess','Reparación creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_reparacion)
    {
        $reparacion=REPARACIONES::join('taxis','cod_taxi','=','id_taxi')
        ->find($id_reparacion);

        return view('gestion.reparaciones.show',compact('reparacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_reparacion)
    {
        $reparacion = REPARACIONES::find($id_reparacion);

        $taxis = TAXIS::where('borrado','=',0)
        ->where('disponibilidad','=',0)->get();

        return view('gestion.reparaciones.Layoutactualizar',compact('reparacion'),compact('taxis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_reparacion)
    {
        $reparacion = REPARACIONES::find($id_reparacion);

        $reparacion->update($request->all());
        return redirect()->route('reparaciones.index')
        ->with('success','Reparción actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_reparacion)
    {
        $reparacion = REPARACIONES::find($id_reparacion);
        $reparacion->borrado=true;
        $reparacion->save();
        
        return redirect()->route('reparaciones.index')
        ->with('success','Reparación borrada correctamente');
    }
}
