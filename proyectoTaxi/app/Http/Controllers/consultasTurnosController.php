<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turnos;


class consultasTurnosController extends Controller
{
    public function consultarTurnoDiario(Request $request){
        
        $turnosDiario = TURNOS::where('borrado','=','0')
        ->where('fecha_inicio','=',$request->fecha_inicio)->get();
        return view('consultas.consultasTurnos.index',compact('turnosDiario'),compact('request'));
    }

    private function inicio_fin_semana($fecha){
        
        $diaInicio="Monday";
        $diaFin="Sunday";
    
        $strFecha = strtotime($fecha);
    
        $fechaInicio = date('Y-m-d',strtotime('last '.$diaInicio,$strFecha));
        $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));
    
        if(date("l",$strFecha)==$diaInicio){
            $fechaInicio= date("Y-m-d",$strFecha);
        }
        if(date("l",$strFecha)==$diaFin){
            $fechaFin= date("Y-m-d",$strFecha);
        }
        return Array("fechaInicio"=>$fechaInicio,"fechaFin"=>$fechaFin);
    }

    public function consultarTurnoSemanal(Request $request){
        $fechaSemana= $this->inicio_fin_semana($request->fecha_inicio);        
        $turnosSemanal = TURNOS::where('borrado','=','0')
        ->where('fecha_inicio','>=',$fechaSemana['fechaInicio'])
        ->where('fecha_inicio','<=',$fechaSemana['fechaFin'])
        ->get();
        
        return view('consultas.consultasTurnos.index',compact('turnosSemanal'),compact('request'));
    }
    public function index(){
        return view('consultas.consultasTurnos.index');
    }
}
