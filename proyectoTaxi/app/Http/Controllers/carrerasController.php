<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Taxis;
use App\Tarifas;
use App\Empleados;
use App\Turnos;
use App\Carreras;
use App\EstadosCarreras;


class carrerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carreras= CARRERAS::orderBy('id_carrera','DESC')
        ->where('borrado','=',0)
        ->paginate(5);
        
        return view('gestion.carreras.index',compact('carreras'))
        ->with((request()->input('page',1)-1)*5);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()    {
          
        $controladores = EMPLEADOS::where('borrado','=',0)
        ->whereNull('num_Licencia')
        ->where('situacion','=','activo')->get();

        $tarifas = TARIFAS::where('borrado','=',0)->get();

        $turnos = TURNOS::where('borrado','=',0)
        ->where('fecha_inicio','=',date('Y-m-d'))
        ->get();


        $datos=[$controladores,$tarifas,$turnos];
        return view('gestion.carreras.Layoutcreate',compact('datos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CARRERAS::create($request->all());
        $carrera =CARRERAS::all()->last(); 
        $estado_carrera = [
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i:s'),
            'path_foto' => '',
            'cod_estado' => 1,
            'cod_carrera' => $carrera->id_carrera
        ];

        $taxi=CARRERAS::join('turnos','id_turno','cod_turno')
        ->join('taxis','id_taxi','cod_taxi')
        ->where('id_turno','=',$request->cod_turno);
        
        $taxi->update(["disponibilidad"=>true]);
        ESTADOSCARRERAS::create($estado_carrera);
        return redirect()->route('carreras.index')
        ->with('sucess','Carrera creado correctamente');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_carrera)
    {        
        $carrera = CARRERAS::find($id_carrera);        
        $tarifa = TARIFAS::find($carrera->cod_tarifa);
        $turno = TURNOS::find($carrera->cod_turno);
        $datos=[$tarifa,$turno];
        if($carrera->cod_controlador){
            $controlador=EMPLEADOS::find($carrera->cod_controlador);
            array_push($datos,$controlador);
        }
        return view('gestion.carreras.show',compact('carrera'),compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_carrera)
    {
        $carrera = CARRERAS::find($id_carrera);   
        
        $controladores = EMPLEADOS::where('borrado','=',0)
        ->whereNull('num_Licencia')
        ->where('situacion','=','activo')->get();

        $tarifas = TARIFAS::where('borrado','=',0)->get();

        $turnos = TURNOS::where('borrado','=',0)->get();
        
        $datos=[$controladores,$tarifas,$turnos];
        return view('gestion.carreras.Layoutactualizar',compact('carrera'),compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_carrera)
    {
        $carrera= CARRERAS::find($id_carrera);        
        $carrera->update($request->all());
        return redirect()->route('carreras.index')
        ->with('success','Carrera actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_carrera)
    {
        $carrera = CARRERAS::find($id_carrera);
        $carrera->borrado=true;
        $carrera->save();
        
        return redirect()->route('carreras.index')
        ->with('success','Carrera borrada correctamente');
    }
}
