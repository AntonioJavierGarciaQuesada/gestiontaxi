<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Taxis;

class consultasEstadoController extends Controller
{
    public function index(){        
        
        return view('consultas.consultasEstado.index');   
    }
    public function consultarAveriados(){
        $taxisReparacion=TAXIS::join('reparaciones','id_taxi','cod_taxi')
        ->where('taxis.borrado','=',0)
        ->where('reparaciones.borrado','=',0)->get();
        return view('consultas.consultasEstado.index',compact('taxisReparacion'));   
    }
    public function consultarOcupados(){
        $taxisOcupados=TAXIS::where('borrado','=',0)
        ->where('disponibilidad','=',true)->get();
        
        return view('consultas.consultasEstado.index',compact('taxisOcupados')); 
    }

}
