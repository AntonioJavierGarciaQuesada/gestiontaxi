<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoEstado;
use Validator;

class tipoEstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipoEstados = TIPOESTADO::where("borrado","=",false)->paginate(5);
        return view('gestion.tipoEstado.index',compact('tipoEstados'))
            ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestion.tipoEstado.Layoutcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'tipo' => 'unique:tipo_estado'
            
        ])->validate();
        TIPOESTADO::create($request->all());
        return redirect()->route('tipoEstado.index')
        ->with('success','El tipo Estado ha sido creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_tipo_estado)
    {
        $tipoEstado = TIPOESTADO::find($id_tipo_estado);
        return view('gestion.tipoEstado.show',compact('tipoEstado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_tipo_estado)
    {
        $tipoEstado = TIPOESTADO::find($id_tipo_estado);
        return view('gestion.tipoEstado.Layoutactualizar',compact('tipoEstado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_tipo_estado)
    {
        $tipoEstado=TIPOESTADO::find($id_tipo_estado);
        
        $tipoEstado->update($request->all());
        return redirect()->route('tipoEstado.index')
            ->with('success','El tipo estado se ha actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_tipo_estado)
    {
        $tipoEstado=TIPOESTADO::find($id_tipo_estado);
        $tipoEstado->borrado=true;
        $tipoEstado->save();
        //TIPOESTADO::find($id_tipo_estado)->delete();
        return redirect()->route('tipoEstado.index')
            ->with('success','El tipo estado se ha borrado correctamente');
    }
}
