<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Carreras;
use App\Turnos;
use App\Taxis;
use Validator;

class consultasCarrerasController extends Controller
{
       
    public function index(){  
        $taxis = TAXIS::where('borrado','=',0)->get(); 
        $carreras = CARRERAS::join('turnos','cod_turno','id_turno')
       ->join('taxis','cod_taxi','id_taxi')
       ->where('turnos.borrado','=',0)
       ->where('carreras.borrado','=',0)->get();           
        $datos=[$taxis,$carreras,null];
        return view('consultas.consultasCarreras.index',compact('datos'));
   }

   public function consultarTaxi(Request $request){
    $validator = Validator::make($request->all(),
    [
        'fecha_inicio' => 'beforeOrEqual:fecha_fin',
        'fecha_fin' => 'afterOrEqual:fecha_inicio',
       
    ])->validate();
    
       $carrerasTaxi = DB::select("SELECT cod_carrera,cod_tarifa,cod_turno,origen,cod_controlador,destino 
       FROM carreras join estados_carrera on id_carrera=cod_carrera
       join turnos on cod_turno = id_turno
       where cod_taxi ='".$request->id_taxi."' and carreras.borrado = 0 and estados_carrera.fecha BETWEEN '".$request->fecha_inicio."' AND '".$request->fecha_fin."'" );
       
       $carreras = CARRERAS::join('turnos','cod_turno','id_turno')
       ->join('taxis','cod_taxi','id_taxi')
       ->where('turnos.borrado','=',0)
       ->where('carreras.borrado','=',0)->get();  
       $taxis = TAXIS::where('borrado','=',0)->get(); 
       
       $datos=[$taxis,$carreras,$request]; 
     
       return view('consultas.consultasCarreras.index',compact('carrerasTaxi'),compact('datos'));
   }

   public function consultarCarrera(Request $request){
   
    $carrera = CARRERAS::join('turnos','cod_turno','id_turno')
    ->join('taxis','cod_taxi','id_taxi')
    ->join('estados_carrera','id_carrera','cod_carrera')
    ->where('carreras.borrado','=',0)
    ->where('id_carrera','=',$request->id_carrera)->get();
    $carreraUnica=$carrera[0];   
    $carreras = CARRERAS::join('turnos','cod_turno','id_turno')
       ->join('taxis','cod_taxi','id_taxi')
       ->where('turnos.borrado','=',0)
       ->where('carreras.borrado','=',0)->get();
       $taxis = TAXIS::where('borrado','=',0)->get(); 
       $datos=[$taxis,$carreras,$request];
    return view('consultas.consultasCarreras.index',compact('datos'),compact('carreraUnica'));
}
}
