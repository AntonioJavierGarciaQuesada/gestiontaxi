<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Empleados;

class empleadosController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
             
            $empleadosCompleto = EMPLEADOS::where('empleados.borrado','=',false)
            ->paginate(5);            
        
        return view('gestion.empleados.index',compact('empleadosCompleto'))
            ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestion.empleados.Layoutcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'num_seg_social' => 'unique:empleados',
                       
        ])->validate(); 
        EMPLEADOS::create($request->all());
        $empleado = EMPLEADOS::all()->last();
        if($request->hasFile('foto')){
            $path="empleado";
            $file = $request->foto;
            $extension = $file->getClientOriginalExtension();
            $fileName = "foto".$empleado->id_empleado.".".$extension;
            $file->move($path,$fileName);
            $empleado->foto="/".$path."/".$fileName;
            $empleado->save();
        }   
                   
        return redirect()->route('empleados.index')
            ->with('success','Empleado creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_empleado)
    {
        $empleado = EMPLEADOS::find($id_empleado);
        return view('gestion.empleados.show',compact('empleado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_empleado)
    {
        $empleado = EMPLEADOS::find($id_empleado);        
        return view('gestion.empleados.Layoutactualizar',compact('empleado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_empleado)
    {
       
        $empleado=EMPLEADOS::find($id_empleado);        
        
        $empleado->update($request->all());        
        return redirect()->route('empleados.index')
            ->with('success','Empleado actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_empleado)
    {
        $empleado=EMPLEADOS::find($id_empleado);
        $empleado->borrado=true;
        $empleado->save();
              
        return redirect()->route('empleados.index')
            ->with('success','Empleado borrado correctamente');
    }

   
}
