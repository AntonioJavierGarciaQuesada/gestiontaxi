<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarifas;
use Validator;

class tarifasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tarifas = TARIFAS::where("borrado","=",false)->paginate(5);        
        return view('gestion.tarifas.index',compact('tarifas'))
            ->with((request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestion.tarifas.Layoutcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'tipo' => 'required'
                                   
        ])->validate(); 
        TARIFAS::create($request->all());
        return redirect()->route('tarifas.index')
        ->with('success','Tarifa creada correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_tarifa)
    {        
        $tarifa = TARIFAS::find($id_tarifa);
        return view('gestion.tarifas.show',compact('tarifa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_tarifa)
    {
        $tarifa = TARIFAS::find($id_tarifa);
        return view('gestion.tarifas.Layoutactualizar',compact('tarifa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_tarifa)
    {
        $tarifa=TARIFAS::find($id_tarifa);
        
        $tarifa->update($request->all());
        return redirect()->route('tarifas.index')
            ->with('success','Tarifa actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_tarifa)
    {
        $tarifa=TARIFAS::find($id_tarifa);
        $tarifa->borrado=true;
        $tarifa->save();
        //TARIFAS::find($id_tarifa)->delete();
        return redirect()->route('tarifas.index')
            ->with('success','Tarifa borrada correctamente');
    }
}
