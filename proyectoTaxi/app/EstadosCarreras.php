<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosCarreras extends Model
{
    protected $table="estados_carrera";
    protected $primaryKey ="id_estado_carrera";
    protected $fillable = [
        'fecha',
        'hora',
        'path_foto',
        'cod_estado',
        'cod_carrera'
    ];
    public $timestamps = false;
}
