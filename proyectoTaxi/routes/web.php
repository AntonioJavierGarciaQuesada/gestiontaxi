<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//---------------------------------------------------- LOGIN ----------------------------------------------
Route::get('/', function () {
    return view('welcome');
});

//------------------------------------------ SECCION CONSULTAS --------------------------------------------
Route::get('/consultas','consultasController');
//------------------------------------------  CONSULTAS TAXIS -------------------------------------------
Route::post('/consultas/consultasTaxi/consultaListadoGeneral','consultasTaxiController@consultarGeneral');
Route::post('/consultas/consultasTaxi/consultaListadoTrabajador','consultasTaxiController@consultarTrabajador');
Route::get('/consultas/consultasTaxi/','consultasTaxiController@index');
//------------------------------------------- CONSULTAS ESTADO ------------------------------------------
Route::post('/consultas/consultasEstado/averiados','consultasEstadoController@consultarAveriados');
Route::post('/consultas/consultasEstado/ocupados','consultasEstadoController@consultarOcupados');
Route::get('/consultas/consultasEstado/','consultasEstadoController@index');
//------------------------------------------- CONSULTAS CARRERAS ------------------------------------------
Route::get('/consultas/consultasCarreras/','consultasCarrerasController@index');
Route::post('/consultas/consultasCarreras/taxi','consultasCarrerasController@consultarTaxi');
Route::post('/consultas/consultasCarreras/carrera','consultasCarrerasController@consultarCarrera');
//------------------------------------------- CONSULTAS INGRESOS ------------------------------------------
Route::get('/consultas/consultasIngresos/','consultasIngresosController@index');
Route::post('/consultas/consultasIngresos/IngresoMedio','consultasIngresosController@consultarIngresoMedio');
Route::post('/consultas/consultasIngresos/IngresoTotal','consultasIngresosController@consultarIngresoTotal');
Route::post('/consultas/consultasIngresos/Productividad','consultasIngresosController@consultarProductividad');
//------------------------------------------- CONSULTAS TURNOS ------------------------------------------
Route::get('/consultas/consultasTurnos/','consultasTurnosController@index');
Route::post('/consultas/consultasTurnos/turnoDiario','consultasTurnosController@consultarTurnoDiario');
Route::post('/consultas/consultasTurnos/turnoSemanal','consultasTurnosController@consultarTurnoSemanal');
//------------------------------------------- CONSULTAS EMPLEADOS ------------------------------------------
Route::get('/consultas/consultasEmpleados/','consultasEmpleadosController@index');
Route::post('/consultas/consultasEmpleados/asentismo','consultasEmpleadosController@consultarAsentismo');
Route::post('/consultas/consultasEmpleados/vacaciones','consultasEmpleadosController@consultarVacaciones');
Route::post('/consultas/consultasEmpleados/facturacionMensual','consultasEmpleadosController@consultarFacturacionMensual');


//------------------------------------------- SECCION GESTIÓN -----------------------------------------------

Route::get('/gestion','gestionController');
Route::get('/gestionTaxista','gestionController');

Route::resource('/gestion/modelos', 'modelosController');

Route::resource('/gestion/tarifas', 'tarifasController');

Route::resource('/gestion/tipoEstado', 'tipoEstadoController');

Route::resource('/gestion/empleados', 'empleadosController');

Route::resource('gestion/taxis', 'taxisController');

Route::get('/gestion/recuperar','recuperarController@index');
Route::post('/gestion/recuperar','recuperarController@edit');
Route::post('/gestion/recuperar/{tabla}/{id}/','recuperarController@updateCarrera($tabla,$id)');

Route::resource('/gestion/reparaciones', 'reparacionesController');

Route::resource('gestion/turnos', 'turnosController');

Route::resource('gestion/carreras', 'carrerasController');

Route::resource('gestion/estadosCarreras', 'estadosCarrerasController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



