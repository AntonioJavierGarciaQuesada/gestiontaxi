<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreras', function (Blueprint $table) {
            $table->increments('id_carrera');
            $table->time('duracion')->nullable();
            $table->float('precio')->nullable();
            $table->enum('tipo_cliente',['cliente_calle','cliente_llamada','cliente_aplicacion','otros']);
            $table->string('origen',200);
            $table->string('destino',200)->nullable();
            $table->string('gps_origen',200);
            $table->string('gps_destino',200)->nullable();
            $table->string('tipo_pago',50)->nullable();
            $table->enum('zona',['zona1','zona2','zona3','zona4','zona5']);
            $table->integer('cod_tarifa')->unsigned();
            $table->integer('cod_turno')->unsigned();
            $table->integer('cod_controlador')->unsigned()->nullable();
            $table->boolean('borrado')->default(false);
            $table->foreign('cod_tarifa')
            ->references('id_tarifa')->on('tarifas');
            $table->foreign('cod_turno')
            ->references('id_turno')->on('turnos');
            $table->foreign('cod_controlador')
            ->references('id_empleado')->on('empleados');
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreras');
    }
}
