<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxis', function (Blueprint $table) {
            $table->increments('id_taxi');
            $table->integer('num_licencia_taxi')->unique();
            $table->string('matricula',10)->unique();
            $table->boolean('estado')->default(false);
            $table->boolean('disponibilidad')->default(false);
            $table->date('year');
            $table->string('propietario');
            $table->boolean('datafono');
            $table->boolean('borrado')->default(false);
            $table->integer('cod_modelo')->unsigned();
            $table->foreign('cod_modelo')
            ->references('id_coche')->on('modelos');
            $table->engine = 'InnoDB';
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxis');
    }
}
