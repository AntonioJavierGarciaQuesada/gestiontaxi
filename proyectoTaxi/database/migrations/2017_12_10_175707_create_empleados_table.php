<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            
            $table->increments('id_empleado');           
            $table->string("num_seg_social",11)->unique();
            $table->enum('puesto',['taxista','controlador']);
            $table->string('num_Licencia',10)->nullable();            
            $table->string('nombre',30);
            $table->string('apellidos',50);
            $table->integer('telefono');
            $table->date('fecha_nacimiento');
            $table->date('fecha_contratacion');
            $table->string('email',30);
            $table->text('direccion');
            $table->enum('situacion',['activo','permiso','vacaciones','baja'])->default('activo');
            $table->string('foto')->nullable();                        
            $table->boolean('borrado')->default(false);
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}

 
