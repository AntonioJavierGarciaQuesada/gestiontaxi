    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosCarreraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_carrera', function (Blueprint $table) {
            $table->increments('id_estado_carrera');
            $table->date('fecha');
            $table->time('hora');
            $table->string('path_foto')->nullable();
            $table->integer('cod_estado')->unsigned();
            $table->integer('cod_carrera')->unsigned();
            $table->boolean('borrado')->default(false);            
            $table->foreign('cod_estado')
            ->references('id_tipo_estado')->on('tipo_estado');
            $table->foreign('cod_carrera')
            ->references('id_carrera')->on('carreras');
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_carrera');
    }
}
