<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReparacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reparaciones', function (Blueprint $table) {
            $table->increments('id_reparacion');
            $table->date('fecha_entrada');
            $table->date('fecha_salida')->nullable();
            $table->date('fecha_prevista')->nullable();
            $table->float('coste_reparacion')->nullable();
            $table->integer('cod_taxi')->unsigned();
            $table->boolean('borrado')->default(false);
            $table->foreign('cod_taxi')
            ->references('id_taxi')->on('taxis');
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparaciones');
    }
}
