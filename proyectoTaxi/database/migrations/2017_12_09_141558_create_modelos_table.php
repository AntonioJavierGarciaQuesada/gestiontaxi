<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->increments('id_coche');
            $table->string('marca',30);
            $table->string('modelo',50);
            $table->smallInteger('num_plazas')->unsigned();
            $table->string('cilindrada',10);
            $table->string('descripcion',50);
            $table->string('foto',30)->nullable();
            $table->boolean('borrado')->default(false);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
}
