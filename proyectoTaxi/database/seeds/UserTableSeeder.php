<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_taxista = Role::where('name', 'taxista')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $user = new user();
        $user->name = 'taxista';
        $user->email = 'taxista@correo.com';
        $user->password = bcrypt('1234');
        $user->save();
        $user->roles()->attach($role_taxista);
        $user = new user();
        $user->name = 'Admin';
        $user->email = 'admin@correo.com';
        $user->password = bcrypt('1234');
        $user->save();
        $user->roles()->attach($role_admin);
    }
}
