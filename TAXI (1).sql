-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-03-2018 a las 10:52:16
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `TAXI`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id_carrera` int(10) UNSIGNED NOT NULL,
  `duracion` time DEFAULT NULL,
  `precio` double(8,2) DEFAULT NULL,
  `tipo_cliente` enum('cliente_calle','cliente_llamada','cliente_aplicacion','otros') COLLATE utf8mb4_unicode_ci NOT NULL,
  `origen` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destino` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gps_origen` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gps_destino` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_pago` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zona` enum('zona1','zona2','zona3','zona4','zona5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `cod_tarifa` int(10) UNSIGNED NOT NULL,
  `cod_turno` int(10) UNSIGNED NOT NULL,
  `cod_controlador` int(10) UNSIGNED DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id_carrera`, `duracion`, `precio`, `tipo_cliente`, `origen`, `destino`, `gps_origen`, `gps_destino`, `tipo_pago`, `zona`, `cod_tarifa`, `cod_turno`, `cod_controlador`, `borrado`) VALUES
(1, NULL, 24.00, 'cliente_calle', 'Calle 1', 'Calle 2', '12341', '12342', 'Contado', 'zona1', 1, 1, NULL, 0),
(2, NULL, 18.00, 'cliente_llamada', 'Calle 2', 'Calle 3', '12342', '12343', 'Contado', 'zona1', 1, 1, 3, 0),
(3, NULL, 8.00, 'cliente_calle', 'Calle 4', 'Calle 5', '12344', '12345', 'Contado', 'zona1', 1, 1, NULL, 0),
(4, NULL, 30.00, 'cliente_calle', 'Calle 21', 'Calle 22', '21341', '21342', 'Contado', 'zona1', 2, 4, NULL, 0),
(5, NULL, 10.00, 'cliente_llamada', 'Calle 22', 'Calle 23', '21342', '21343', 'Contado', 'zona1', 2, 4, NULL, 0),
(6, NULL, 17.00, 'cliente_calle', 'Calle 24', 'Calle 25', '21344', '21345', 'Contado', 'zona1', 2, 4, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id_empleado` int(10) UNSIGNED NOT NULL,
  `num_seg_social` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `puesto` enum('taxista','controlador') COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_Licencia` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `situacion` enum('activo','permiso','vacaciones','baja') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activo',
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id_empleado`, `num_seg_social`, `puesto`, `num_Licencia`, `nombre`, `apellidos`, `telefono`, `fecha_nacimiento`, `fecha_contratacion`, `email`, `direccion`, `situacion`, `foto`, `borrado`) VALUES
(1, '12345678901', 'taxista', '12345678A', 'Antonio Javier', 'Garcia Quesada', 672328832, '1979-10-09', '2018-03-06', 'taxista@correo.com', 'calle aguado  nº30 5A Fuengirola Málaga', 'vacaciones', '/empleado/foto1.jpeg', 0),
(2, '12312312347', 'taxista', '12345678B', 'Ricardo', 'Garcia Gomez', 673123456, '2000-03-01', '2018-03-08', 'taxista2@correo.com', 'c\\inventada n10 3ºc', 'activo', NULL, 0),
(3, '12312312343', 'controlador', NULL, 'María', 'Garcia Flores', 672328390, '1985-09-01', '2018-03-01', 'controlador@correo.com', 'calle salido gomez n10 6A Málaga', 'activo', '/empleado/foto3.jpeg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_carrera`
--

CREATE TABLE `estados_carrera` (
  `id_estado_carrera` int(10) UNSIGNED NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `path_foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_estado` int(10) UNSIGNED NOT NULL,
  `cod_carrera` int(10) UNSIGNED NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados_carrera`
--

INSERT INTO `estados_carrera` (`id_estado_carrera`, `fecha`, `hora`, `path_foto`, `cod_estado`, `cod_carrera`, `borrado`) VALUES
(1, '2018-03-12', '09:03:06', '', 1, 1, 0),
(2, '2018-03-12', '09:04:04', '', 1, 2, 0),
(3, '2018-03-12', '09:04:44', '', 1, 3, 0),
(4, '2018-03-12', '09:05:35', '', 1, 4, 0),
(5, '2018-03-12', '09:06:23', '', 1, 5, 0),
(6, '2018-03-12', '09:07:22', '', 1, 6, 0),
(7, '2018-03-12', '08:20:00', NULL, 2, 1, 0),
(8, '2018-03-12', '09:00:00', NULL, 2, 2, 0),
(9, '2018-03-12', '16:30:00', NULL, 2, 4, 0),
(10, '2018-03-12', '17:00:00', NULL, 2, 5, 0),
(11, '2018-03-12', '09:40:00', NULL, 2, 3, 0),
(12, '2018-03-13', '17:40:00', NULL, 2, 6, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_09_141558_create_modelos_table', 1),
(4, '2017_12_10_142911_create_tarifas_table', 1),
(5, '2017_12_10_155259_create_tipo_estado_table', 1),
(6, '2017_12_10_175707_create_empleados_table', 1),
(7, '2017_12_15_155211_create_taxis_table', 1),
(8, '2017_12_16_060724_create_turnos_table', 1),
(9, '2017_12_16_113253_create_carreras_table', 1),
(10, '2017_12_20_073017_create_estados_carrera_table', 1),
(11, '2018_01_20_190119_create_reparaciones_table', 1),
(12, '2018_03_10_105207_create_roles_table', 1),
(13, '2018_03_10_105439_create_role_user_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos`
--

CREATE TABLE `modelos` (
  `id_coche` int(10) UNSIGNED NOT NULL,
  `marca` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_plazas` smallint(5) UNSIGNED NOT NULL,
  `cilindrada` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `modelos`
--

INSERT INTO `modelos` (`id_coche`, `marca`, `modelo`, `num_plazas`, `cilindrada`, `descripcion`, `foto`, `borrado`) VALUES
(1, 'Seat', 'Ateca tdi 115CV', 5, '1.9', 'Coche leasing. No adaptado.', '/modelos/foto1.jpg', 0),
(2, 'Toyota', 'Priusplus Eco 2018', 3, '2.0', 'Coche electrico/hibrido. Garantía de 3 años', '/modelos/foto2.jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reparaciones`
--

CREATE TABLE `reparaciones` (
  `id_reparacion` int(10) UNSIGNED NOT NULL,
  `fecha_entrada` date NOT NULL,
  `fecha_salida` date DEFAULT NULL,
  `fecha_prevista` date DEFAULT NULL,
  `coste_reparacion` double(8,2) DEFAULT NULL,
  `cod_taxi` int(10) UNSIGNED NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reparaciones`
--

INSERT INTO `reparaciones` (`id_reparacion`, `fecha_entrada`, `fecha_salida`, `fecha_prevista`, `coste_reparacion`, `cod_taxi`, `borrado`) VALUES
(1, '2018-03-12', '2018-03-15', NULL, 300.00, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrador', '2018-03-12 07:07:16', '2018-03-12 07:07:16'),
(2, 'taxista', 'Taxista', '2018-03-12 07:07:16', '2018-03-12 07:07:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-03-12 07:07:16', '2018-03-12 07:07:16'),
(2, 1, 2, '2018-03-12 07:07:16', '2018-03-12 07:07:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifas`
--

CREATE TABLE `tarifas` (
  `id_tarifa` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio_minuto` double(8,2) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tarifas`
--

INSERT INTO `tarifas` (`id_tarifa`, `tipo`, `descripcion`, `precio_minuto`, `borrado`) VALUES
(1, 'Mañana', 'Horario 8:00-16:00', 0.90, 0),
(2, 'Tarde', 'Horario 16:00-24:00', 1.00, 0),
(3, 'Nocturna', 'Horario 00:00-8:0', 1.20, 0),
(4, 'Especial', 'Aeropuerto', 1.30, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taxis`
--

CREATE TABLE `taxis` (
  `id_taxi` int(10) UNSIGNED NOT NULL,
  `num_licencia_taxi` int(11) NOT NULL,
  `matricula` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  `disponibilidad` tinyint(1) NOT NULL DEFAULT '0',
  `year` date NOT NULL,
  `propietario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datafono` tinyint(1) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `cod_modelo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `taxis`
--

INSERT INTO `taxis` (`id_taxi`, `num_licencia_taxi`, `matricula`, `estado`, `disponibilidad`, `year`, `propietario`, `datafono`, `borrado`, `cod_modelo`) VALUES
(1, 12345678, '1111-AAA', 1, 0, '2010-10-10', 'Empresa', 1, 0, 1),
(2, 12345679, '1111-BBB', 0, 0, '2018-03-12', 'Empresa', 1, 0, 2),
(3, 12345671, '1111-CCC', 0, 0, '2018-03-05', 'Empresa', 1, 0, 2),
(4, 12345673, '1111-DDD', 0, 0, '2018-03-01', 'Empresa', 1, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado`
--

CREATE TABLE `tipo_estado` (
  `id_tipo_estado` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_estado`
--

INSERT INTO `tipo_estado` (`id_tipo_estado`, `tipo`, `descripcion`, `borrado`) VALUES
(1, 'Iniciar', 'La carrera se inicia cuando el cliente se sube al taxi', 0),
(2, 'Finalizar', 'La carrera se finaliza cuando el taxi llega al lugar precisado por el cliente', 0),
(3, 'Avería', 'Situación anómala llamar al siguiente número 678907655', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos`
--

CREATE TABLE `turnos` (
  `id_turno` int(10) UNSIGNED NOT NULL,
  `fecha_inicio` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `fecha_fin` date NOT NULL,
  `hora_fin` time NOT NULL,
  `hora_inicio_real` time DEFAULT NULL,
  `hora_fin_real` time DEFAULT NULL,
  `cod_taxista` int(10) UNSIGNED NOT NULL,
  `cod_taxi` int(10) UNSIGNED NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `turnos`
--

INSERT INTO `turnos` (`id_turno`, `fecha_inicio`, `hora_inicio`, `fecha_fin`, `hora_fin`, `hora_inicio_real`, `hora_fin_real`, `cod_taxista`, `cod_taxi`, `borrado`) VALUES
(1, '2018-03-12', '08:00:00', '2018-03-12', '16:00:00', '08:20:00', '16:20:00', 1, 2, 0),
(2, '2018-03-13', '08:00:00', '2018-03-13', '16:00:00', NULL, NULL, 1, 2, 0),
(3, '2018-03-14', '08:00:00', '2018-03-14', '16:00:00', NULL, NULL, 1, 2, 0),
(4, '2018-03-12', '16:00:00', '2018-03-12', '23:30:00', NULL, NULL, 2, 2, 0),
(5, '2018-03-13', '16:00:00', '2018-03-13', '23:30:00', NULL, NULL, 2, 2, 0),
(6, '2018-03-14', '16:00:00', '2018-03-14', '23:30:00', NULL, NULL, 2, 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'taxista', 'taxista@correo.com', '$2y$10$GhPPSO8UFgW7wMmQvUtgB.UalpTy7gm/xCgWfcyhVlfVU//8HgR8m', NULL, '2018-03-12 07:07:16', '2018-03-12 07:07:16'),
(2, 'Admin', 'admin@correo.com', '$2y$10$/9sVtJkp8EB7qd7hrBSGsueJBoRwYMzW.WhypMKoG8IZZK.mIO9oO', NULL, '2018-03-12 07:07:16', '2018-03-12 07:07:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id_carrera`),
  ADD KEY `carreras_cod_tarifa_foreign` (`cod_tarifa`),
  ADD KEY `carreras_cod_turno_foreign` (`cod_turno`),
  ADD KEY `carreras_cod_controlador_foreign` (`cod_controlador`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id_empleado`),
  ADD UNIQUE KEY `empleados_num_seg_social_unique` (`num_seg_social`);

--
-- Indices de la tabla `estados_carrera`
--
ALTER TABLE `estados_carrera`
  ADD PRIMARY KEY (`id_estado_carrera`),
  ADD KEY `estados_carrera_cod_estado_foreign` (`cod_estado`),
  ADD KEY `estados_carrera_cod_carrera_foreign` (`cod_carrera`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`id_coche`);

--
-- Indices de la tabla `reparaciones`
--
ALTER TABLE `reparaciones`
  ADD PRIMARY KEY (`id_reparacion`),
  ADD KEY `reparaciones_cod_taxi_foreign` (`cod_taxi`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifas`
--
ALTER TABLE `tarifas`
  ADD PRIMARY KEY (`id_tarifa`);

--
-- Indices de la tabla `taxis`
--
ALTER TABLE `taxis`
  ADD PRIMARY KEY (`id_taxi`),
  ADD UNIQUE KEY `taxis_num_licencia_taxi_unique` (`num_licencia_taxi`),
  ADD UNIQUE KEY `taxis_matricula_unique` (`matricula`),
  ADD KEY `taxis_cod_modelo_foreign` (`cod_modelo`);

--
-- Indices de la tabla `tipo_estado`
--
ALTER TABLE `tipo_estado`
  ADD PRIMARY KEY (`id_tipo_estado`);

--
-- Indices de la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`id_turno`),
  ADD KEY `turnos_cod_taxista_foreign` (`cod_taxista`),
  ADD KEY `turnos_cod_taxi_foreign` (`cod_taxi`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id_carrera` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id_empleado` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `estados_carrera`
--
ALTER TABLE `estados_carrera`
  MODIFY `id_estado_carrera` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `modelos`
--
ALTER TABLE `modelos`
  MODIFY `id_coche` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `reparaciones`
--
ALTER TABLE `reparaciones`
  MODIFY `id_reparacion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tarifas`
--
ALTER TABLE `tarifas`
  MODIFY `id_tarifa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `taxis`
--
ALTER TABLE `taxis`
  MODIFY `id_taxi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tipo_estado`
--
ALTER TABLE `tipo_estado`
  MODIFY `id_tipo_estado` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `turnos`
--
ALTER TABLE `turnos`
  MODIFY `id_turno` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD CONSTRAINT `carreras_cod_controlador_foreign` FOREIGN KEY (`cod_controlador`) REFERENCES `empleados` (`id_empleado`),
  ADD CONSTRAINT `carreras_cod_tarifa_foreign` FOREIGN KEY (`cod_tarifa`) REFERENCES `tarifas` (`id_tarifa`),
  ADD CONSTRAINT `carreras_cod_turno_foreign` FOREIGN KEY (`cod_turno`) REFERENCES `turnos` (`id_turno`);

--
-- Filtros para la tabla `estados_carrera`
--
ALTER TABLE `estados_carrera`
  ADD CONSTRAINT `estados_carrera_cod_carrera_foreign` FOREIGN KEY (`cod_carrera`) REFERENCES `carreras` (`id_carrera`),
  ADD CONSTRAINT `estados_carrera_cod_estado_foreign` FOREIGN KEY (`cod_estado`) REFERENCES `tipo_estado` (`id_tipo_estado`);

--
-- Filtros para la tabla `reparaciones`
--
ALTER TABLE `reparaciones`
  ADD CONSTRAINT `reparaciones_cod_taxi_foreign` FOREIGN KEY (`cod_taxi`) REFERENCES `taxis` (`id_taxi`);

--
-- Filtros para la tabla `taxis`
--
ALTER TABLE `taxis`
  ADD CONSTRAINT `taxis_cod_modelo_foreign` FOREIGN KEY (`cod_modelo`) REFERENCES `modelos` (`id_coche`);

--
-- Filtros para la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD CONSTRAINT `turnos_cod_taxi_foreign` FOREIGN KEY (`cod_taxi`) REFERENCES `taxis` (`id_taxi`),
  ADD CONSTRAINT `turnos_cod_taxista_foreign` FOREIGN KEY (`cod_taxista`) REFERENCES `empleados` (`id_empleado`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
